// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Layer.hpp"

#ifndef __cnf_Configuration_hpp__
#define __cnf_Configuration_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     class Configuration {

     public:
	  static Configuration& get();

	  Layer& layer();

	  Configuration& operator<<(const Layer&);

     public:
	  Configuration(const Configuration&) = delete;
	  Configuration(Configuration&&) = delete;
	  Configuration& operator=(const Configuration&) = delete;
	  Configuration& operator=(Configuration&&) = delete;

     private:
	  static Configuration* cnf;
	  Layer cnf_layer;

     private:
	  Configuration();
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_Configuration_hpp__
