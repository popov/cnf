// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Value.hpp"
#include "Level.hpp"

#ifndef __cnf_Layer_hpp__
#define __cnf_Layer_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     class Layer {

     public:
	  Layer(level_t);

	  dict_t dict;

	  Layer& operator<<(const Layer&);

     private:
	  const level_t level;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_Layer_hpp__
