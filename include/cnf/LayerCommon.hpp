// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Layer.hpp"
#include <ostream>
#include <list>

#ifndef __cnf_LayerCommon_hpp__
#define __cnf_LayerCommon_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     using path_t = std::list<string_t>;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     void dump(const Layer&, std::ostream&);
     void str(const Layer&, std::ostream&);
     bool build(std::istream&, Layer&);
     void get(const Layer&, value_t&, path_t);
     void get(const dict_t&, value_t&, path_t);
     void get(value_t, value_t&, path_t);
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_LayerCommon_hpp__
