// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <string>
#include <cstdint>

#ifndef __cnf_Types_hpp__
#define __cnf_Types_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     using string_t = std::string;
     using key_t    = string_t;
     using int_t    = int64_t;
     using float_t  = double;
     using bool_t   = bool;
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_Types_hpp__
