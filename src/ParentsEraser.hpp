// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <vector>
#include <map>
#include <list>

#ifndef __cnf_ParentsEraser_hpp__
#define __cnf_ParentsEraser_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     template <class P, class Value>
     class ParentsEraser {

     public:
	  ParentsEraser(std::vector<P> p,
			std::map<std::vector<P>, Value>& v):
	       keys(p),
	       container(v) { return; }

	  void erase() {
	       find_keys();
	       erase_keys();
	  }

     private:
	  std::vector<P> keys;
	  std::map<std::vector<P>, Value>& container;

     private:
	  using keys_for_erase_t = std::list<std::vector<P>>;
	  keys_for_erase_t kfe;

	  void erase_keys() const {
	       for (auto& i : kfe) {
		    container.erase(i);
	       }
	  }

	  void find_keys() {
	       auto i = std::begin(container);
	       auto end = std::end(container);
	       while (i != end) {
		    auto k = i->first;
		    if (compare(k))
			 kfe.push_back(k);
		    i++;
	       }
	  }

	  bool compare(std::vector<P> p) const {
	       if (p.size() <= keys.size()) return false;
	       auto it = std::begin(p);
	       auto en = it + keys.size();
	       std::vector<P> temp(it, en);
	       if (temp == keys)
		    return true;
	       return false;
	  };
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_ParentsEraser_hpp__
