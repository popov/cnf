// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "KeyHolder.hpp"

//////////////////////////////////////////////////////////////////
void cnf::KeyHolder::push(key k) {
     keys.push_back(k);
}

void cnf::KeyHolder::release() {
     if (empty()) return;
     keys.pop_back();
}

bool cnf::KeyHolder::empty() const {
     return keys.empty();
}

cnf::string_t cnf::KeyHolder::str() const {
     if (empty()) return "";
     string_t result;
     bool no_first = false;
     for (auto i : keys) {
	  if (no_first) result += ".";
	  if (has_dot(i)) add_quot(i);
	  result += i;
	  no_first = true;
     }
     return result;
}

bool cnf::KeyHolder::has_dot(const key& k) const {
     auto n = k.find('.');
     if (n == std::string::npos) return false;
     return true;
}

void cnf::KeyHolder::add_quot(key& k) const {
     auto temp = "\"" + k + "\"";
     k = temp;
}
//////////////////////////////////////////////////////////////////
