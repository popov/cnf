// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Events.hpp"
#include <sstream>
#include <iomanip>

namespace cnf::event {
//////////////////////////////////////////////////////////////////
     static long calc_num_width(const events_t& evs) {
	  std::stringstream ss;
	  ss << (evs.size() - 1);
	  auto sss = ss.str();
	  return sss.size();
     }

     static void put_num(std::stringstream& ss,
			 long num, long width) {
	  ss <<
	       std::setfill('0')
	     <<
	       std::setw(width)
	     <<
	       num;
     }

     static void put(std::stringstream& ss, char c) {
	  ss << c;
     }

     static void put(std::stringstream& ss, std::string s) {
	  ss << s;
     }

     static void put_space(std::stringstream& ss) {
	  put(ss, ' ');
     }

     static void put_enter(std::stringstream& ss) {
	  put(ss, '\n');
     }

     dump_t dump(const events_t& evs) {
	  auto w = calc_num_width(evs);
	  std::stringstream ss;
	  long k = 0;
	  for (auto& i : evs) {
	       put_num(ss, k, w);
	       put_space(ss);
	       put(ss, i->str());
	       put_enter(ss);
	       k++;
	  }
	  return ss.str();
     }
//////////////////////////////////////////////////////////////////
}
