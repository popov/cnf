// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "ArrayMaker.hpp"

//////////////////////////////////////////////////////////////////
void cnf::ArrayMaker::make(values_t& vs, array_size n) {
     auto result = get<value_t>(Array());
     Array* resulta = (Array*)(result.get());
     array_t* ar = &(resulta->content);
     if (n == 0) return;
     for (std::size_t i = 0; i < n; i++) {
	  if (vs.size() == 0) break;
	  auto v = vs.back();
	  vs.pop_back();
	  auto it = std::begin(*ar);
	  ar->insert(it, v);
	  resulta->a_type = v->type;
     }
     vs.push_back(result);
}
//////////////////////////////////////////////////////////////////
