// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Merging.hpp"

//////////////////////////////////////////////////////////////////
void cnf::merge(value_t src, level_t srcl,
		value_t trg, level_t trgl) {
     if (! check_dict(src, trg)) return;
     merge_dict(src, srcl, trg, trgl);
}

void cnf::merge_dict(value_t src, level_t srcl,
		     value_t trg, level_t trgl) {
     auto ks = get_keys(src, trg);
     auto no_joint_keys = std::get<0>(ks);
     auto joint_keys = std::get<1>(ks);
     copy(no_joint_keys, src, trg);
     auto ks_t = get_type_keys(src, trg, joint_keys);
     auto same_type_keys = std::get<0>(ks_t);
     auto other_type_keys = std::get<1>(ks_t);
     copy(other_type_keys, src, srcl, trg, trgl);
     auto ks_dict_nodict = get_dict_no_dict(src, same_type_keys);
     auto dict_keys = std::get<0>(ks_dict_nodict);
     auto no_dict_keys = std::get<1>(ks_dict_nodict);
     copy(no_dict_keys, src, srcl, trg, trgl);
     merge(src, srcl, trg, trgl, dict_keys);
}

bool cnf::check_dict(value_t l, value_t r) {
     if (check_dict(l))
	  if (check_dict(r)) return true;
     return false;
}

bool cnf::check_dict(value_t l) {
     if (l->type == Value::DICT)
	  return true;
     return false;
}

cnf::keys2_t cnf::get_keys(value_t s, value_t t) {
     keys_t key_no;
     keys_t key_ex;
     dict_t& d = get_dict(s);
     for (auto& i : d) {
	  auto k = i.first;
	  if (key_in_dict(k, t))
	       key_ex.push_back(k);
	  else
	       key_no.push_back(k);
     }
     return { key_no, key_ex };
}

bool cnf::key_in_dict(key_t k, value_t v) {
     dict_t& dict = get_dict(v);
     if (dict.count(k) == 0)
	  return false;
     return true;
}

void cnf::copy(const keys_t& ks, value_t s, value_t t) {
     dict_t& dicts = get_dict(s);
     dict_t& dictt = get_dict(t);
     for (auto& i : ks) {
	  dictt[i] = dicts[i];
     }
}

void cnf::copy(const keys_t& ks,
	       value_t src, level_t srcl,
	       value_t trg, level_t trgl) {
     if (srcl > trgl) copy(ks, src, trg);
}

cnf::keys2_t
cnf::get_type_keys(value_t s, value_t t, const keys_t& ks) {
     keys_t key_same;
     keys_t key_other;
     dict_t& dicts = get_dict(s);
     dict_t& dictt = get_dict(t);
     for (auto& i : ks) {
	  if (dicts.at(i)->type == dictt.at(i)->type)
	       key_same.push_back(i);
	  else
	       key_other.push_back(i);
     }
     return { key_same, key_other };
}

cnf::keys2_t
cnf::get_dict_no_dict(value_t v, const keys_t& ks) {
     keys_t key_dict;
     keys_t key_nodict;
     dict_t& dict = get_dict(v);
     for (auto& i : ks) {
	  auto val = dict.at(i);
	  if (check_dict(val))
	       key_dict.push_back(i);
	  else
	       key_nodict.push_back(i);
     }
     return { key_dict, key_nodict };
}

void cnf::merge(value_t src, level_t srcl,
		value_t trg, level_t trgl,
		const keys_t& ks) {
     dict_t& dicts = get_dict(src);
     dict_t& dictt = get_dict(trg);
     for (auto& i : ks) {
	  auto vs = dicts.at(i);
	  auto vt = dictt.at(i);
	  merge_dict(vs, srcl, vt, trgl);
     }
}

cnf::dict_t& cnf::get_dict(value_t v) {
     Dict* d = (Dict*)(v.get());
     return d->content;
}
//////////////////////////////////////////////////////////////////
