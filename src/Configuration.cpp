// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <cnf/Configuration.hpp>

//////////////////////////////////////////////////////////////////
cnf::Configuration* cnf::Configuration::cnf = nullptr;

cnf::Configuration& cnf::Configuration::get() {
     if (cnf != nullptr) return *cnf;
     cnf = new Configuration;
     return *cnf;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::Configuration::Configuration():
     cnf_layer(0)
{
     return;
}

cnf::Layer& cnf::Configuration::layer() {
     return cnf_layer;
}

cnf::Configuration&
cnf::Configuration::operator<<(const Layer& l) {
     cnf_layer << l;
     return (*this);
}
//////////////////////////////////////////////////////////////////
