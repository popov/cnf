// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <string>
#include <list>

#ifndef __cnf_KeyHolder_hpp__
#define __cnf_KeyHolder_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     using string_t = std::string;
     using key      = string_t;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class KeyHolder {

     public:
	  void push(key);
	  void release();
	  bool empty() const;
	  string_t str() const;

     private:
	  using keys_t = std::list<key>;
	  keys_t keys;

	  bool has_dot(const key&) const;
	  void add_quot(key&) const;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_KeyHolder_hpp__
