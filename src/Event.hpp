// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <string>
#include <vector>

#ifndef __cnf_event_Event_hpp__
#define __cnf_event_Event_hpp__

namespace cnf::event {
//////////////////////////////////////////////////////////////////
     using key_t = std::string;
     using keys_t = std::vector<key_t>;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     enum Type {
	  START,
	  FINISH,
	  COMMENT,
	  TABLES_ARRAY,
	  TABLE,
	  KEY,
	  START_INLINE,
	  FINISH_INLINE,
	  START_ARRAY,
	  FINISH_ARRAY,
	  STRING,
	  BOOLEAN,
	  DATE_TIME,
	  INTEGER,
	  FLOAT,
     };
//////////////////////////////////////////////////////////////////
     
//////////////////////////////////////////////////////////////////
     struct Event {
	  Event(Type t): type(t) { return; }
	  virtual ~Event() = 0;

	  const Type type;

	  virtual std::string str() const = 0;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct Start: public Event {
	  Start(): Event(START) { return; }
	  virtual ~Start() { return; }

	  virtual std::string str() const {
	       return "START";
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct Finish: public Event {
	  Finish(): Event(FINISH) { return; }
	  virtual ~Finish() { return; }

	  virtual std::string str() const {
	       return "FINISH";
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct StartInline: public Event {
	  StartInline(): Event(START_INLINE) { return; }
	  virtual ~StartInline() { return; }

	  virtual std::string str() const {
	       return "START_INLINE";
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct StartArray: public Event {
	  StartArray(): Event(START_ARRAY) { return; }
	  virtual ~StartArray() { return; }

	  virtual std::string str() const {
	       return "START_ARRAY";
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct StringContent: public Event {
	  StringContent(Type t, std::string s):
	       Event(t),
	       content(s) { return; }
	  virtual ~StringContent() = 0;

	  const std::string content;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct DateTime: public StringContent {
	  DateTime(std::string s):
	       StringContent(DATE_TIME, s) { return; }
	  virtual ~DateTime() { return; }

	  virtual std::string str() const {
	       return "DATE_TIME: " + content;
	  }
     };
//////////////////////////////////////////////////////////////////
     
//////////////////////////////////////////////////////////////////
     struct Comment: public StringContent {
	  Comment(std::string s):
	       StringContent(COMMENT, s) { return; }
	  virtual ~Comment() { return; }

	  virtual std::string str() const {
	       return "COMMENT: " + content;
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct String: public StringContent {
	  String(std::string s):
	       StringContent (STRING, s) { return; }
	  virtual ~String() { return; }

	  virtual std::string str() const {
	       return "STRING: " + content;
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct Keys: public Event {
	  Keys(Type t, keys_t s):
	       Event(t),
	       content(s) { return; }
	  virtual ~Keys() = 0;

	  const keys_t content;

	  virtual std::string str() const = 0;

	  virtual std::string get_keys() const {
	       std::string result("");
	       bool no_first = false;
	       for (auto& i : content) {
		    if (no_first)
			 result += '/';
		    result += i;
		    no_first = true;
	       }
	       return result;
	  }
     };
//////////////////////////////////////////////////////////////////
     
//////////////////////////////////////////////////////////////////
     struct TablesArray: public Keys {
	  TablesArray(keys_t s):
	       Keys(TABLES_ARRAY, s) { return; }
	  virtual ~TablesArray() { return; }

	  virtual std::string str() const {
	       return "TABLES_ARRAY: " + get_keys();
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct Table: public Keys {
	  Table(keys_t s):
	       Keys(TABLE, s) { return; }
	  virtual ~Table() { return; }

	  virtual std::string str() const {
	       return "TABLE: " + get_keys();
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct Key: public Event {
	  Key(key_t s):
	       Event(KEY),
	       content(s) { return; }
	  virtual ~Key() { return; }

	  const key_t content;

	  virtual std::string str() const {
	       return "KEY: " + content;
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct FinishInline: public Event {
	  FinishInline(std::size_t s):
	       Event(FINISH_INLINE),
	       content(s) { return; }
	  virtual ~FinishInline() { return; }

	  const std::size_t content;

	  virtual std::string str() const {
	       return "FINISH_INLINE: " + std::to_string(content);
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct FinishArray: public Event {
	  FinishArray(std::size_t s):
	       Event(FINISH_ARRAY),
	       content(s) { return; }
	  virtual ~FinishArray() { return; }

	  const std::size_t content;

	  virtual std::string str() const {
	       return "FINISH_ARRAY: " + std::to_string(content);
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct Boolean: public Event {
	  Boolean(bool s):
	       Event(BOOLEAN),
	       content(s) { return; }
	  virtual ~Boolean() { return; }

	  const bool content;

	  virtual std::string str() const {
	       return "BOOLEAN: " + (content)? "true" : "false";
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct Integer: public Event {
	  Integer(std::int64_t s):
	       Event(INTEGER),
	       content(s) { return; }
	  virtual ~Integer() { return; }

	  const std::int64_t content;

	  virtual std::string str() const {
	       return "INTEGER: " + std::to_string(content);
	  }
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     struct Float: public Event {
	  Float(double s):
	       Event(FLOAT),
	       content(s) { return; }
	  virtual ~Float() { return; }
	  
	  const double content;

	  virtual std::string str() const {
	       return "FLOAT: " + std::to_string(content);
	  }
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_event_Event_hpp__
