// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Events.hpp"
#include <loltoml/parse.hpp>
#include <string>

#ifndef __cnf_Handler2_hpp__
#define __cnf_Handler2_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     class Hndlr {

     public:
	  enum state_t { UNDEF, ERROR_PARSER, ERROR_STREAM, OK, };
	  struct status_t {
	       state_t state;
	       std::string msg;
	  };

     public:
	  Hndlr(event::events_t&, std::istream&);
	  virtual ~Hndlr() = 0;

	  virtual status_t parse();

     protected:
	  void save(event::event_t);
	  event::keys_t get_keys(loltoml::key_iterator_t begin,
				 loltoml::key_iterator_t end) const;
     private:
	  event::events_t& evs;
	  std::istream& stream;
	  
     protected:
	  void init();

     public:
	  // --- callbacks ---
	  virtual void start_document() = 0;
	  virtual void finish_document() = 0;
	  virtual void comment(const std::string &comment) = 0;
	  virtual void array_table(loltoml::key_iterator_t begin,
				   loltoml::key_iterator_t end) = 0;
	  virtual void table(loltoml::key_iterator_t begin,
			     loltoml::key_iterator_t end) = 0;
	  virtual void key(const std::string &key) = 0;
	  virtual void start_inline_table() = 0;
	  virtual void finish_inline_table(std::size_t table_size) = 0;
	  virtual void start_array() = 0;
	  virtual void finish_array(std::size_t array_size) = 0;
	  virtual void string(const std::string &value) = 0;
	  virtual void boolean(bool value) = 0;
	  virtual void datetime(const std::string &value) = 0;
	  virtual void integer(std::int64_t value) = 0;
	  virtual void floating_point(double value) = 0;
	  // --- callbacks ---
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class Handler: public Hndlr {

     public:
	  Handler(event::events_t&, std::istream&);
	  virtual ~Handler();

	  // --- callbacks ---
	  virtual void start_document();
	  virtual void finish_document();
	  virtual void comment(const std::string &comment);
	  virtual void array_table(loltoml::key_iterator_t begin,
				   loltoml::key_iterator_t end);
	  virtual void table(loltoml::key_iterator_t begin,
			     loltoml::key_iterator_t end);
	  virtual void key(const std::string &key);
	  virtual void start_inline_table();
	  virtual void finish_inline_table(std::size_t table_size);
	  virtual void start_array();
	  virtual void finish_array(std::size_t array_size);
	  virtual void string(const std::string &value);
	  virtual void boolean(bool value);
	  virtual void datetime(const std::string &value);
	  virtual void integer(std::int64_t value);
	  virtual void floating_point(double value);
	  // --- callbacks ---
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_Handler2_hpp__
