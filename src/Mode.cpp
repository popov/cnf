// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Mode.hpp"

//////////////////////////////////////////////////////////////////
cnf::Mode::Mode() {
     reset();
}

void cnf::Mode::reset() {
     while (! state.empty())
	  state.pop();
}

bool cnf::Mode::table() const {
     if (state.empty()) return true;
     return false;
}

bool cnf::Mode::array() const {
     if (state.empty()) return false;
     if (state.top() == ARRAY) return true;
     return false;
}

bool cnf::Mode::inlin() const {
     if (state.empty()) return false;
     if (state.top() == INLINE) return true;
     return false;
}

void cnf::Mode::release() {
     if (state.empty()) return;
     state.pop();
}

void cnf::Mode::to_array() {
     state.push(ARRAY);
}

void cnf::Mode::to_inline() {
     state.push(INLINE);
}
//////////////////////////////////////////////////////////////////
