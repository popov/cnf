// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Event.hpp"
#include <memory>
#include <list>

#ifndef __cnf_event_Events_hpp__
#define __cnf_event_Events_hpp__

namespace cnf::event {
//////////////////////////////////////////////////////////////////
     using event_t = std::shared_ptr<Event>;
     using events_t = std::list<event_t>;
     using dump_t = std::string;

     dump_t dump(const events_t&);
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_event_Events_hpp__
