// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <cnf/Value.hpp>
#include <cnf/Level.hpp>
#include <list>
#include <tuple>

#ifndef __cnf_Merging_hpp__
#define __cnf_Merging_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     using keys_t = std::list<key_t>;
     using keys2_t = std::tuple<keys_t, keys_t>;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     void merge(value_t src, level_t srcl,
		value_t trg, level_t trgl);
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     void merge_dict(value_t src, level_t srcl,
		     value_t trg, level_t trgl);
     bool check_dict(value_t, value_t);
     bool check_dict(value_t);
     keys2_t get_keys(value_t, value_t);
     keys2_t get_type_keys(value_t, value_t, const keys_t&);
     bool key_in_dict(key_t, value_t);
     void copy(const keys_t&, value_t, value_t);
     void copy(const keys_t&, value_t src, level_t srcl,
	       value_t trg, level_t trgl);
     void merge(value_t src, level_t srcl,
		value_t trg, level_t trgl,
		const keys_t&);
     keys2_t get_dict_no_dict(value_t, const keys_t&);
     dict_t& get_dict(value_t);
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_Merging_hpp__
