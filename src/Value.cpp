// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <cnf/Value.hpp>

//////////////////////////////////////////////////////////////////
cnf::Value::Value(Type t):
     type(t)
{
     return;
}

cnf::Value::~Value() {
     return;
}

bool cnf::Value::is_scalar() const {
     if (type == STRING) return true;
     if (type == INT) return true;
     if (type == FLOAT) return true;
     if (type == BOOL) return true;
     return false;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::String::String(string_t s):
     Value(STRING),
     content(s)
{
     return;
}

cnf::string_t cnf::String::str() const {
     return ("\"" + content + "\"");
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::Int::Int(int_t s):
     Value(INT),
     content(s)
{
     return;
}

cnf::string_t cnf::Int::str() const {
     return std::to_string(content);
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::Float::Float(float_t s):
     Value(FLOAT),
     content(s)
{
     return;
}

cnf::string_t cnf::Float::str() const {
     return std::to_string(content);
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::Bool::Bool(bool_t s):
     Value(BOOL),
     content(s)
{
     return;
}

cnf::string_t cnf::Bool::str() const {
     if (content) return "true";
     return "false";
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::Dict::Dict():
     Value(DICT)
{
     return;
}

cnf::string_t cnf::Dict::str() const {
     //
     //TODO
     //
     return "DICT";
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::Array::Array():
     Value(ARRAY),
     a_type(STRING)
{
     return;
}

cnf::string_t cnf::Array::str() const {
     return "ARRAY";
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
namespace cnf {

     template<>
     value_t get<value_t>(String s) {
	  return std::make_shared<String>(s.content);
     }

     template<>
     value_t get<value_t>(Int s) {
	  return std::make_shared<Int>(s.content);
     }

     template<>
     value_t get<value_t>(Float s) {
	  return std::make_shared<Float>(s.content);
     }

     template<>
     value_t get<value_t>(Bool s) {
	  return std::make_shared<Bool>(s.content);
     }

     template<>
     value_t get<value_t>(Dict s) {
	  return std::make_shared<Dict>(s);
     }

     template<>
     value_t get<value_t>(Array s) {
	  return std::make_shared<Array>(s);
     }

     template<>
     Dict get(value_t vt) {
	  auto r = dynamic_cast<Dict*>(vt.get());
	  if (r == nullptr) return Dict();
	  return *r;
     }

     template<>
     Dict* get(value_t vt) {
	  auto r = dynamic_cast<Dict*>(vt.get());
	  return r;
     }

     template<>
     Array get(value_t vt) {
	  auto r = dynamic_cast<Array*>(vt.get());
	  if (r == nullptr) return Array();
	  return *r;
     }

     template<>
     Array* get(value_t vt) {
	  auto r = dynamic_cast<Array*>(vt.get());
	  return r;
     }

     template<>
     String get(value_t vt) {
	  auto r = dynamic_cast<String*>(vt.get());
	  if (r == nullptr) return String("");
	  return *r;
     }

     template<>
     String* get(value_t vt) {
	  auto r = dynamic_cast<String*>(vt.get());
	  return r;
     }

     template<>
     Int get(value_t vt) {
	  auto r = dynamic_cast<Int*>(vt.get());
	  if (r == nullptr) return Int(0);
	  return *r;
     }

     template<>
     Int* get(value_t vt) {
	  auto r = dynamic_cast<Int*>(vt.get());
	  return r;
     }

     template<>
     Float get(value_t vt) {
	  auto r = dynamic_cast<Float*>(vt.get());
	  if (r == nullptr) return Float(0);
	  return *r;
     }

     template<>
     Float* get(value_t vt) {
	  auto r = dynamic_cast<Float*>(vt.get());
	  return r;
     }

     template<>
     Bool get(value_t vt) {
	  auto r = dynamic_cast<Bool*>(vt.get());
	  if (r == nullptr) return Bool(false);
	  return *r;
     }

     template<>
     Bool* get(value_t vt) {
	  auto r = dynamic_cast<Bool*>(vt.get());
	  return r;
     }
}
//////////////////////////////////////////////////////////////////
