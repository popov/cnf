// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Stringer.hpp"

//////////////////////////////////////////////////////////////////
const char cnf::Stringer::space    = ' ';
const char cnf::Stringer::eq       = '=';
const char cnf::Stringer::cr       = '\r';
const char cnf::Stringer::lf       = '\n';
const char cnf::Stringer::left_br  = '[';
const char cnf::Stringer::right_br = ']';
const char cnf::Stringer::comma    = ',';
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::Stringer::Stringer(MODE m, shift_t s, OUT_MODE om):
     mode(m),
     shift(s),
     out_mode(om)
{
     return;
}

cnf::Stringer::Stringer(MODE m, shift_t s,
			const KeyHolder& k, OUT_MODE om):
     mode(m),
     shift(s),
     kh(k),
     out_mode(om)
{
     return;
}

cnf::Stringer::~Stringer() {
     return;
}

void cnf::Stringer::fill_key(key_t k, std::ostream& ss) const {
     ss << k;
     ss << space;
     ss << eq;
     ss << space;
}

void cnf::Stringer::fill_cap(key_t k, std::ostream& ss) const {
     ss << left_br;
     ss << k;
     ss << right_br;
}

void cnf::Stringer::fill_cap_ad(key_t k, std::ostream& ss) const {
     ss << left_br;
     fill_cap(k, ss);
     ss << right_br;
}

void cnf::Stringer::fill_left_br(std::ostream& ss) const {
     ss << left_br;
}

void cnf::Stringer::fill_right_br(std::ostream& ss) const {
     ss << right_br;
}

void cnf::Stringer::fill_comma(std::ostream& ss) const {
     ss << comma;
}

void cnf::Stringer::fill_space(std::ostream& ss) const {
     ss << space;
}

void cnf::Stringer::fill_shift(std::ostream& ss) const {
     for (unsigned char i = 0; i < shift; i++) {
	  ss << space;
     }
}

void cnf::Stringer::fill_enter(std::ostream& ss) const {
     if (mode == WIN)
	  ss << cr;
     ss << lf;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::DictStringer::DictStringer(MODE m, shift_t s, OUT_MODE om):
     Stringer(m, s, om)
{
     return;

}

cnf::DictStringer::DictStringer(MODE m, shift_t s,
				const KeyHolder& k, OUT_MODE om):
     Stringer(m, s, k, om)
{
     return;
}

void cnf::DictStringer::fill(const dict_t& d, std::ostream& ss) {
     for (auto i : d) {
	  auto key = i.first;
	  auto val = i.second;
	  if (check_scalar(val)) {
	       s_keys.push_back(key);
	       continue;
	  }
	  if (check_dict(val)) {
	       d_keys.push_back(key);
	       continue;
	  }
	  if (check_array(val)) {
	       a_keys.push_back(key);
	       continue;
	  }
     }
     ScalarStringer SS(mode, shift, out_mode);
     SS.fill(s_keys, d, ss);

     ArrayStringer AS(mode, shift, kh, out_mode);
     AS.fill(a_keys, d, ss);

     DictStringer DS(mode, shift, kh, out_mode);
     DS.fill(d_keys, d, ss);
}

void cnf::DictStringer::fill
(dict_k k, const dict_t& d, std::ostream& ss) {
     if (k.size() == 0) return;
     k.sort();
     for (auto i : k) {
	  kh.push(i);
	  if (out_mode == REGULAR) {
	       if (! can_hide(d, i)) {
		    fill_enter(ss);
		    fill_cap(kh.str(), ss);
		    fill_enter(ss);
	       }
	  } else {
	       fill_enter(ss);
	       fill_cap(kh.str(), ss);
	       fill_enter(ss);
	  }
	  DictStringer DS(mode, shift, kh, out_mode);
	  DS.fill(d.at(i), ss);
	  kh.release();
     }
}

void cnf::DictStringer::fill(value_t v, std::ostream& ss) {
     auto d = get<Dict>(v);
     fill(d.content, ss);
}

bool cnf::DictStringer::check_scalar(value_t v) const {
     if (v->type == Value::STRING) return true;
     if (v->type == Value::INT) return true;
     if (v->type == Value::FLOAT) return true;
     if (v->type == Value::BOOL) return true;
     return false;
}

bool cnf::DictStringer::check_dict(value_t v) const {
     if (v->type == Value::DICT) return true;
     return false;
}

bool cnf::DictStringer::check_array(value_t v) const {
     if (v->type == Value::ARRAY) return true;
     return false;
}

bool cnf::DictStringer::can_hide(const dict_t& d, key_t k) const {
     auto dd = d.at(k);
     Dict* dict = (Dict*)(dd.get());
     auto it = std::begin(dict->content);
     auto end = std::end(dict->content);
     while (it != end) {
	  auto val = (*it).second;
	  if (check_scalar(val)) return false;
	  if (check_array(val)) {
	       Array* ar = (Array*)(val.get());
	       if (ar->a_type != Value::DICT) return false;
	  }
	  it++;
     }
     return true;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::ScalarStringer::ScalarStringer(MODE m, shift_t s, OUT_MODE om):
     Stringer(m, s, om)
{
     return;
}

void cnf::ScalarStringer::
fill(scalar_k sk, const dict_t& d, std::ostream& ss) const {
     if (sk.size() == 0) return;
     sk.sort();
     for (auto i : sk) {
	  fill_key(i, ss);
	  fill_val(d.at(i), ss);
	  fill_enter(ss);
     }
}

void cnf::ScalarStringer::
fill_val(value_t v, std::ostream& ss) const {
     ss << v->str();
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::ArrayStringer::ArrayStringer(MODE m, shift_t s, OUT_MODE om):
     Stringer(m, s, om)
{
     return;
}

cnf::ArrayStringer::ArrayStringer(MODE m, shift_t s,
				  const KeyHolder& k, OUT_MODE om):
     Stringer(m, s, k, om)
{
     return;
}

void cnf::ArrayStringer::
fill(array_k ak, const dict_t& d, std::ostream& ss) {
     if (ak.size() == 0) return;
     ak.sort();
     for (auto i : ak) {
	  auto key = i;
	  auto val = d.at(key);
	  auto a = get<Array>(val);
	  fill(key, a, ss);
     }
}

void cnf::ArrayStringer::fill(key_t k,
			      Array a,
			      std::ostream& ss) {
     if (array_scalar(a)) {
	  scalar(k, a, ss);
	  return;
     }
     if (array_array(a)) {
	  array(k, a, ss);
	  return;
     }
     if (array_dict(a)) {
	  dict(k, a, ss);
	  return;
     }
}

void cnf::ArrayStringer::
scalar(key_t k, Array a, std::ostream& ss) const {
     fill_enter(ss);
     fill_key(k, ss);
     out_scalar(a, ss);
     fill_enter(ss);
}

void
cnf::ArrayStringer::
out_scalar(Array a, std::ostream& ss) const {
     fill_left_br(ss);
     fill_enter(ss);
     for (auto j : a.content) {
	  fill_shift(ss);
	  ss << j->str();
	  fill_comma(ss);
	  fill_enter(ss);
     }
     fill_right_br(ss);
}

void
cnf::ArrayStringer::
out_array(Array a, std::ostream& ss) const {
     if (array_scalar(a)) {
          out_scalar(a, ss);
	  return;
     }
     if (array_array(a)) {
          array(a, ss);
	  return;
     }
}

void cnf::ArrayStringer::
array(key_t k, Array a, std::ostream& ss) const {
     fill_enter(ss);
     fill_key(k, ss);
     array(a, ss);
}

void cnf::ArrayStringer::
dict(key_t k, Array a, std::ostream& ss) {
     kh.push(k);
     for (auto j : a.content) {
	  auto d = get<Dict>(j);
	  fill_enter(ss);
	  fill_cap_ad(kh.str(), ss);
	  fill_enter(ss);
	  DictStringer DS(mode, shift, kh, out_mode);
	  DS.fill(d.content, ss);
     }
     kh.release();
}

void cnf::ArrayStringer::
array(Array a, std::ostream& ss) const {
     fill_left_br(ss);
     fill_space(ss);
     for (auto j : a.content) {
	  auto b = get<Array>(j);
	  out_array(b, ss);
	  fill_comma(ss);
	  fill_space(ss);
     }
     fill_right_br(ss);
     fill_enter(ss);
}

bool cnf::ArrayStringer::array_scalar(Array v) const {
     if (v.a_type == Value::STRING) return true;
     if (v.a_type == Value::INT) return true;
     if (v.a_type == Value::FLOAT) return true;
     if (v.a_type == Value::BOOL) return true;
     return false;
}

bool cnf::ArrayStringer::array_array(Array v) const {
     if (v.a_type == Value::ARRAY) return true;
     return false;
}

bool cnf::ArrayStringer::array_dict(Array v) const {
     if (v.a_type == Value::DICT) return true;
     return false;
}
//////////////////////////////////////////////////////////////////
