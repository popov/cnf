// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "BuilderCommon.hpp"

//////////////////////////////////////////////////////////////////
cnf::BuilderBase::BuilderBase(dict_t& d, parents_t& p):
     dict(d),
     parents(p)
{
     return;
}

void cnf::BuilderBase::save_to_root(value_t v, path_chunk_t p) {
     dict[p] = v;
}

void cnf::BuilderBase::save_to_(value_t cnt,
				value_t v,
				path_chunk_t p) const {
     if (cnt->type == Value::DICT) {
	  save_to_dict(cnt, v, p);
	  
     } else if (cnt->type == Value::ARRAY) {
	  save_to_array(cnt, v, p);
     }
}

void cnf::BuilderBase::save_to_array(value_t cnt,
				     value_t v,
				     path_chunk_t p) const {
     if (v->type == Value::ARRAY)
	  return save_array_to_array(cnt, v, p);
     Array* a = (Array*)(cnt.get());
     auto c = a->content.back();
     a->a_type = v->type;
     save_to_dict(c, v, p);
}

void cnf::BuilderBase::save_array_to_array
(value_t cnt, value_t v, path_chunk_t p) const {
     Array* a = (Array*)(cnt.get());
     auto lstd = a->content.back();
     save_to_dict(lstd, v, p);
}

void cnf::BuilderBase::save_to_dict(value_t cnt,
				    value_t v,
				    path_chunk_t p) const {
     Dict* a = (Dict*)(cnt.get());
     a->content[p] = v;
}
//////////////////////////////////////////////////////////////////
