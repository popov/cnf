// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "BuilderCommon.hpp"

#ifndef __cnf_TableBuilder_hpp__
#define __cnf_TableBuilder_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     class TableBuilder: public BuilderBase {

     public:
	  TableBuilder(dict_t&, parents_t&);

	  virtual void build(path_t);
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_TableBuilder_hpp__
