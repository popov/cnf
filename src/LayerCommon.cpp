// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <cnf/LayerCommon.hpp>
#include "Stringer.hpp"
#include "Handler.hpp"
#include "Builder.hpp"

//////////////////////////////////////////////////////////////////
void cnf::dump(const Layer& l, std::ostream& s) {
     DictStringer DS(Stringer::UNIX, 2, Stringer::DUMP);
     DS.fill(l.dict, s);
}

void cnf::str(const Layer& l, std::ostream& s) {
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(l.dict, s);
}

bool cnf::build(std::istream& is, Layer& l) {
     event::events_t evs;
     Handler H(evs, is);
     auto status = H.parse();
     if (status.state != Handler::OK) return false;
     Builder B(l.dict, evs);
     B.build();
     return true;
}

void cnf::get(const Layer& l, value_t& v, path_t p) {
     get(l.dict, v, p);
}

void cnf::get(const dict_t& d, value_t& v, path_t p) {
     auto dp = std::make_shared<Dict>();
     Dict* ddp = (Dict*)(dp.get());
     ddp->content = d;
     get(dp, v, p);
}

void cnf::get(value_t vd, value_t& v, path_t p) {
     if (vd->type != Value::DICT) return;
     auto it = std::begin(p);
     auto end = std::end(p);
     if (it == end) return;
     auto val = (*it);
     Dict* d = (Dict*)(vd.get());
     if (d->content.count(val) == 0) return;
     auto res = d->content.at(val);
     it++;
     if (it == end) {
	  v = res;
	  return;
     }
     p.assign(it, end);
     get(res, v, p);
}
//////////////////////////////////////////////////////////////////
