// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Handler.hpp"

//////////////////////////////////////////////////////////////////
cnf::Hndlr::Hndlr(event::events_t& d, std::istream& i):
     evs(d),
     stream(i)
{
     return;
}

cnf::Hndlr::~Hndlr() {
     return;
}

cnf::Hndlr::status_t cnf::Hndlr::parse() {
     try {
	  loltoml::parse(stream, *this);

     } catch (loltoml::stream_error_t& e) {
	  return { ERROR_STREAM, e.what() };

     } catch (loltoml::parser_error_t& e) {
	  return { ERROR_PARSER, e.what() };

     } catch (...) {
	  return { UNDEF, "" };
     }
     return { OK, "" };
}

void cnf::Hndlr::init() {
     evs.clear();
};

void cnf::Hndlr::save(event::event_t e) {
     evs.push_back(e);
}

cnf::event::keys_t
cnf::Hndlr::get_keys(loltoml::key_iterator_t begin,
		     loltoml::key_iterator_t end) const {
     return event::keys_t(begin, end);
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnf::Handler::Handler(event::events_t& d, std::istream& i):
     Hndlr(d, i)
{
     return;
}

cnf::Handler::~Handler() {
     return;
}
// --- callbacks ---
void cnf::Handler::start_document() {
     init();
     auto s = std::make_shared<event::Start>();
     save(s);
}

void cnf::Handler::finish_document() {
     auto s = std::make_shared<event::Finish>();
     save(s);
}

void cnf::Handler::comment(const std::string& ss) {
     auto s = std::make_shared<event::Comment>(ss);
     save(s);
}

void cnf::Handler::array_table(loltoml::key_iterator_t begin,
			       loltoml::key_iterator_t end) {
     auto s = std::make_shared<event::TablesArray>(get_keys(begin, end));
     save(s);
}

void cnf::Handler::table(loltoml::key_iterator_t begin,
			 loltoml::key_iterator_t end) {
     auto s = std::make_shared<event::Table>(get_keys(begin, end));
     save(s);
}

void cnf::Handler::key(const std::string& key) {
     auto s = std::make_shared<event::Key>(key);
     save(s);
}

void cnf::Handler::start_inline_table() {
     auto s = std::make_shared<event::StartInline>();
     save(s);
}

void cnf::Handler::finish_inline_table(std::size_t ss) {
     auto s = std::make_shared<event::FinishInline>(ss);
     save(s);
}

void cnf::Handler::start_array() {
     auto s = std::make_shared<event::StartArray>();
     save(s);
}

void cnf::Handler::finish_array(std::size_t ss) {
     auto s = std::make_shared<event::FinishArray>(ss);
     save(s);
}

void cnf::Handler::string(const std::string& value) {
     auto s = std::make_shared<event::String>(value);
     save(s);
}

void cnf::Handler::boolean(bool value) {
     auto s = std::make_shared<event::Boolean>(value);
     save(s);
}

void cnf::Handler::datetime(const std::string& value) {
     auto s = std::make_shared<event::DateTime>(value);
     save(s);
}

void cnf::Handler::integer(std::int64_t value) {
     auto s = std::make_shared<event::Integer>(value);
     save(s);
}

void cnf::Handler::floating_point(double value) {
     auto s = std::make_shared<event::Float>(value);
     save(s);
}
// --- callbacks ---
//////////////////////////////////////////////////////////////////
