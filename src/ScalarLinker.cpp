// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "ScalarLinker.hpp"

//////////////////////////////////////////////////////////////////
cnf::ScalarLinker::ScalarLinker(dict_t& d, const parents_t& p):
     dict(d),
     parents(p)
{
     return;
}

void cnf::ScalarLinker::link(const path_t& p,
			     key_t k,
			     values_t& vs) const {
     auto v = vs.back();
     vs.pop_back();
     if (p.size() == 0)
	  return link_root(k, v);
     link(p, k, v);
}

void cnf::ScalarLinker::link_root(key_t k, value_t v) const {
     dict[k] = v;
}

void cnf::ScalarLinker::link(const path_t& p,
			     key_t k,
			     value_t v) const {
     if (parents.count(p) == 0) return;
     auto cnt = parents.at(p);
     if (cnt->type == Value::DICT) {
	  link_to_dict(cnt, k, v);
	  
     } else if (cnt->type == Value::ARRAY) {
	  link_to_array(cnt, k, v);
     }
}

void cnf::ScalarLinker::link_to_dict(value_t cnt,
				     key_t k,
				     value_t v) const {
     Dict* a = (Dict*)(cnt.get());
     a->content[k] = v;
}

void cnf::ScalarLinker::link_to_array(value_t cnt,
				      key_t k,
				      value_t v) const {
     Array* a = (Array*)(cnt.get());
     auto c = a->content.back();
     link_to_dict(c, k, v);
}
//////////////////////////////////////////////////////////////////
