// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <cnf/Value.hpp>
#include <vector>

#ifndef __cnf_ArrayMaker_hpp__
#define __cnf_ArrayMaker_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     using values_t   = std::vector<value_t>;
     using array_size = std::size_t;
//////////////////////////////////////////////////////////////////
     
//////////////////////////////////////////////////////////////////
     class ArrayMaker {

     public:
	  static void make(values_t&, array_size);
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_ArrayMaker_hpp__
