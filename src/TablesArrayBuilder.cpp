// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "TablesArrayBuilder.hpp"
#include "ParentsEraser.hpp"

//////////////////////////////////////////////////////////////////
cnf::TablesArrayBuilder::TablesArrayBuilder(dict_t& d,
					    parents_t& ps,
					    const path_t& p):
     BuilderBase(d, ps),
     prev_path_arr(p)
{
     return;
}

void cnf::TablesArrayBuilder::build(path_t path) {
     container.reset();
     path_t cpath = {};
     path_t prev_path = {};
     std::size_t pos = 0;
     std::size_t array_name_pos = path.size() - 1;
     for (auto& current : path) {
	  cpath.push_back(current);
	  if (parents.count(cpath) == 0) {
	       make_container(array_name_pos, pos);
	       link_container(prev_path, current);
	       save_to_parents(cpath);
	       if (pos == array_name_pos)
		    add_dict_in_array(path);
	  } else {
	       if (pos == array_name_pos) {
		    if (prev_path_arr.size () >= path.size()) {
			 add_dict_in_array(path);
			 ParentsEraser<path_chunk_t, value_t> PE(path, parents);
			 PE.erase();
		    }
	       }
	  }
	  prev_path = cpath;
	  pos++;
     }
}

void cnf::TablesArrayBuilder::add_dict_in_array(path_t path) const {
     auto array = parents.at(path);
     auto t = std::make_shared<Dict>();
     Array* a = (Array*)(array.get());
     a->content.push_back(t);
     a->a_type = Value::DICT;
}

void cnf::TablesArrayBuilder::make_container(std::size_t arr_pos,
					     std::size_t pos) {
     if (pos == arr_pos) {
	  container = std::make_shared<Array>();
	  Array* a = (Array*)(container.get());
	  a->a_type = Value::DICT;

     } else {
	  container = std::make_shared<Dict>();
     }
}

void cnf::TablesArrayBuilder::link_container(const path_t& prev,
					     path_chunk_t current) {
     if (prev.size() == 0)
	  save_to_root(container, current);
     else
	  save_to_(parents.at(prev), container, current);
}

void cnf::TablesArrayBuilder::save_to_parents(const path_t& p) {
     parents[p] = container;
}
//////////////////////////////////////////////////////////////////
