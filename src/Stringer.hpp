// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "KeyHolder.hpp"
#include <cnf/Value.hpp>
#include <list>
#include <ostream>

#ifndef __cnf_Stringer_hpp__
#define __cnf_Stringer_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     using shift_t = unsigned char;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class Stringer {

     public:
	  enum MODE { UNIX, WIN };
	  enum OUT_MODE { REGULAR, DUMP };

     public:
	  Stringer(MODE, shift_t, OUT_MODE);
	  Stringer(MODE, shift_t, const KeyHolder&, OUT_MODE);
	  virtual ~Stringer() = 0;

     protected:
	  const MODE mode;
	  const shift_t shift;

     protected:
	  using key_list = std::list<key_t>;
	  using scalar_k = key_list;
	  using array_k  = key_list;
	  using dict_k   = key_list;

     protected:
	  void fill_key(key_t, std::ostream&) const;
	  void fill_cap(key_t, std::ostream&) const;
	  void fill_cap_ad(key_t, std::ostream&) const;
	  void fill_enter(std::ostream&) const;
	  void fill_left_br(std::ostream&) const;
	  void fill_right_br(std::ostream&) const;
	  void fill_comma(std::ostream&) const;
	  void fill_shift(std::ostream&) const;
	  void fill_space(std::ostream&) const;

     protected:
	  KeyHolder kh;
	  const OUT_MODE out_mode;

     private:
	  static const char space;
	  static const char eq;
	  static const char cr;
	  static const char lf;
	  static const char left_br;
	  static const char right_br;
	  static const char comma;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class DictStringer: public Stringer {

     public:
	  DictStringer(Stringer::MODE, shift_t, OUT_MODE);
	  DictStringer(Stringer::MODE, shift_t, const KeyHolder&, OUT_MODE);

	  void fill(const dict_t&, std::ostream&);

     private:
	  scalar_k s_keys;
	  array_k  a_keys;
	  dict_k   d_keys;

	  void fill(value_t, std::ostream&);
	  void fill(dict_k, const dict_t&, std::ostream&);

	  bool check_scalar(value_t) const;
	  bool check_dict(value_t) const;
	  bool check_array(value_t) const;

	  bool can_hide(const dict_t&, key_t) const;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class ScalarStringer: public Stringer {

     public:
	  ScalarStringer(Stringer::MODE, shift_t, OUT_MODE);

	  void fill(scalar_k, const dict_t&, std::ostream&) const;

     private:
	  void fill_val(value_t, std::ostream&) const;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class ArrayStringer: public Stringer {

     public:
	  ArrayStringer(Stringer::MODE, shift_t, OUT_MODE);
	  ArrayStringer(Stringer::MODE, shift_t,
			const KeyHolder&, OUT_MODE);

	  void fill(array_k, const dict_t&, std::ostream&);

     private:
	  bool array_scalar(Array) const;
	  bool array_array(Array) const;
	  bool array_dict(Array) const;

     private:
	  void fill(key_t, Array, std::ostream&);
	  void scalar(key_t, Array, std::ostream&) const;
	  void array(key_t, Array, std::ostream&) const;
	  void array(Array, std::ostream&) const;
	  void dict(key_t, Array, std::ostream&);
	  void out_scalar(Array, std::ostream&) const;
	  void out_array(Array, std::ostream&) const;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_Stringer_hpp__
