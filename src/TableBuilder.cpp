// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "TableBuilder.hpp"

//////////////////////////////////////////////////////////////////
cnf::TableBuilder::TableBuilder(dict_t& d,
				parents_t& ps):
     BuilderBase(d, ps)
{
     return;
}

void cnf::TableBuilder::build(path_t path) {
     path_t cpath = {};
     path_t prev_path = {};
     for (auto& current : path) {
	  cpath.push_back(current);
	  if (parents.count(cpath) == 0) {
	       auto t = std::make_shared<Dict>();
	       if (prev_path.size() == 0) {
		    save_to_root(t, current);

	       } else {
		    save_to_(parents.at(prev_path), t, current);
	       }
	       parents[cpath] = t;
	  }
	  prev_path = cpath;
     }
}
//////////////////////////////////////////////////////////////////
