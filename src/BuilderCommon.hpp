// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <cnf/Value.hpp>
#include <string>
#include <vector>
#include <map>

#ifndef __cnf_BuilderCommon_hpp__
#define __cnf_BuilderCommon_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     using string_t     = std::string;
     using path_chunk_t = string_t;
     using path_t       = std::vector<path_chunk_t>;
     using parents_t    = std::map<path_t, value_t>;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class BuilderBase {

     public:
	  BuilderBase(dict_t&, parents_t&);

	  virtual void build(path_t) = 0;

     protected:
	  dict_t& dict;
	  parents_t& parents;

     protected:
	  void save_to_root(value_t, path_chunk_t);
	  void save_to_(value_t, value_t, path_chunk_t) const;
	  void save_to_dict(value_t, value_t, path_chunk_t) const;
	  void save_to_array(value_t, value_t, path_chunk_t) const;
	  void save_array_to_array(value_t, value_t, path_chunk_t) const;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_BuilderCommon_hpp__
