// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Builder.hpp"
#include "TableBuilder.hpp"
#include "TablesArrayBuilder.hpp"
#include "ScalarLinker.hpp"
#include "ArrayMaker.hpp"

//////////////////////////////////////////////////////////////////
cnf::Builder::Builder(dict_t& d, const event::events_t& e):
     dict(d),
     evs(e)
{
     return;
}

cnf::Builder::~Builder() {
     clear();
}
     
void cnf::Builder::build() {
     auto i = std::begin(evs);
     auto end = std::end(evs);
     while (i != end) {
	  auto ev = (*i);
	  auto type = ev->type;
	  using namespace event;
	  switch (type) {
	  case (START):
	       start();
	       break;

	  case (FINISH):
	       return finish();

	  case (COMMENT):
	       //
	       // PASS
	       //
	       break;

	  case (TABLES_ARRAY):
	       tables_array(ev);
	       break;

	  case (TABLE):
	       table(ev);
	       break;

	  case (KEY):
	       key(ev);
	       break;

	  case (START_INLINE):
	       mode.to_inline();
	       inlin();
	       break;

	  case (FINISH_INLINE):
	       mode.release();
	       finish_inlin(ev);
	       break;

	  case (START_ARRAY):
	       mode.to_array();
	       break;

	  case (FINISH_ARRAY):
	       mode.release();
	       make_array(ev);
	       link_array();
	       break;

	  case (STRING):
	       make_str(ev);
	       link_scalar();
	       break;

	  case (BOOLEAN):
	       make_bool(ev);
	       link_scalar();
	       break;

	  case (DATE_TIME):
	       make_str(ev);
	       link_scalar();
	       break;

	  case (INTEGER):
	       make_int(ev);
	       link_scalar();
	       break;

	  case (FLOAT):
	       make_float(ev);
	       link_scalar();
	       break;
	  }
	  i++;
     }
     return;
}

void cnf::Builder::start() {
     dict.clear();
     clear();
     mode.reset();
     value_store.clear();
}

void cnf::Builder::finish() {
     clear();
     value_store.clear();
}

void cnf::Builder::key(event::event_t e) {
     auto ev = e.get();
     event::Key* k = (event::Key*)ev;
     last_key = k->content;
}

void cnf::Builder::table(event::event_t e) {
     auto ev = e.get();
     event::Table* t = (event::Table*)ev;
     last_path = t->content;
     TableBuilder TB(dict, parents);
     TB.build(last_path);
}

void cnf::Builder::inlin() {
     last_path.push_back(last_key);
     TableBuilder TB(dict, parents);
     TB.build(last_path);
}

void cnf::Builder::finish_inlin(event::event_t) {
     if (! last_path.empty()) last_path.pop_back();
}

void cnf::Builder::tables_array(event::event_t e) {
     auto ev = e.get();
     event::TablesArray* t = (event::TablesArray*)ev;
     last_path = t->content;
     TablesArrayBuilder TB(dict, parents, prev_path_arr);
     TB.build(last_path);
     prev_path_arr = last_path;
}

void cnf::Builder::clear() {
     last_path.clear();
     last_key.clear();
     parents.clear();
}

void cnf::Builder::link_scalar_to_table() {
     ScalarLinker SL(dict, parents);
     SL.link(last_path, last_key, value_store);
}

void cnf::Builder::link_array() {
     if (mode.array()) return;
     link_scalar_to_table();
}

void cnf::Builder::link_scalar() {
     if (mode.array()) return;
     link_scalar_to_table();
}

void cnf::Builder::make_float(event::event_t e) {
     event::Float* ev = (event::Float*)(e.get());
     auto v = get<value_t>(Float(ev->content));
     save_value(v);
}

void cnf::Builder::make_int(event::event_t e) {
     event::Integer* ev = (event::Integer*)(e.get());
     auto v = get<value_t>(Int(ev->content));
     save_value(v);
}

void cnf::Builder::make_str(event::event_t e) {
     event::String* ev = (event::String*)(e.get());
     auto v = get<value_t>(String(ev->content));
     save_value(v);
}

void cnf::Builder::make_bool(event::event_t e) {
     event::Boolean* ev = (event::Boolean*)(e.get());
     auto v = get<value_t>(Bool(ev->content));
     save_value(v);
}

void cnf::Builder::save_value(value_t v) {
     value_store.push_back(v);
}

void cnf::Builder::make_array(event::event_t e) {
     event::FinishArray* fa = (event::FinishArray*)(e.get());
     ArrayMaker::make(value_store, fa->content);
}
//////////////////////////////////////////////////////////////////
