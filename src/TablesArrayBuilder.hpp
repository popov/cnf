// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "BuilderCommon.hpp"

#ifndef __cnf_TablesArrayBuilder_hpp__
#define __cnf_TablesArrayBuilder_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     class TablesArrayBuilder: public BuilderBase {

     public:
	  TablesArrayBuilder(dict_t&, parents_t&, const path_t&);

	  virtual void build(path_t);

     private:
	  void add_dict_in_array(path_t) const;
	  const path_t& prev_path_arr;

     private:
	  value_t container;
	  void make_container(std::size_t, std::size_t);
	  void link_container(const path_t&, path_chunk_t);
	  void save_to_parents(const path_t&);
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_TablesArrayBuilder_hpp__
