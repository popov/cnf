// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <stack>

#ifndef __cnf_Mode_hpp__
#define __cnf_Mode_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     class Mode {

     public:
	  Mode();

	  void to_inline();
	  void to_array();
	  void release();
	  void reset();

	  bool table() const;
	  bool array() const;
	  bool inlin() const;

     private:
	  enum state_t { TABLE, ARRAY, INLINE, };
	  using State = std::stack<state_t>;
	  State state;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_Mode_hpp__
