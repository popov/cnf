// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Event.hpp"

//////////////////////////////////////////////////////////////////
cnf::event::Event::~Event() {
     return;
}

cnf::event::StringContent::~StringContent() {
     return;
}

cnf::event::Keys::~Keys() {
     return;
}
//////////////////////////////////////////////////////////////////
