// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Merging.hpp"
#include <cnf/Layer.hpp>

//////////////////////////////////////////////////////////////////
cnf::Layer::Layer(level_t l):
     level(l)
{
     return;
}

cnf::Layer&
cnf::Layer::operator<<(const Layer& l) {
     auto target = std::make_shared<Dict>();
     target->content = this->dict;
     auto source = std::make_shared<Dict>();
     source->content = l.dict;
     merge(source, l.level, target, level);
     dict = target->content;
     return *this;
}
//////////////////////////////////////////////////////////////////
