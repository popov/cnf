// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include <cnf/Value.hpp>
#include <string>
#include <vector>
#include <map>

#ifndef __cnf_ScalarLinker_hpp__
#define __cnf_ScalarLinker_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     using string_t      = std::string;
     using path_chunk_t  = string_t;
     using key_t         = string_t;
     using path_t        = std::vector<path_chunk_t>;
     using parents_t     = std::map<path_t, value_t>;
     using values_t      = std::vector<value_t>;
//////////////////////////////////////////////////////////////////
     
//////////////////////////////////////////////////////////////////
     class ScalarLinker {

     public:
	  ScalarLinker(dict_t&, const parents_t&);

	  void link(const path_t&, key_t, values_t&) const;

     private:
	  dict_t& dict;
	  const parents_t& parents;

	  void link_root(key_t, value_t) const;
	  void link(const path_t&, key_t, value_t) const;
	  void link_to_dict(value_t, key_t, value_t) const;
	  void link_to_array(value_t, key_t, value_t) const;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_ScalarLinker_hpp__
