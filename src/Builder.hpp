// -----------------------------------------------------
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
// -----------------------------------------------------

#include "Events.hpp"
#include "Mode.hpp"
#include <cnf/Value.hpp>
#include <map>

#ifndef __cnf_Builder_hpp__
#define __cnf_Builder_hpp__

namespace cnf {
//////////////////////////////////////////////////////////////////
     class Builder {

     public:
	  Builder(dict_t&, const event::events_t&);
	  ~Builder();

	  void build();

     private:
	  dict_t& dict;
	  const event::events_t& evs;

     private:
	  void start();
	  void finish();
	  void key(event::event_t);
	  void table(event::event_t);
	  void tables_array(event::event_t);
	  void make_float(event::event_t);
	  void make_array(event::event_t);
	  void make_int(event::event_t);
	  void make_str(event::event_t);
	  void make_bool(event::event_t);
	  void link_scalar();
	  void link_array();
	  void link_scalar_to_table();
	  void save_value(value_t);
	  void inlin();
	  void finish_inlin(event::event_t);

     private:
	  event::keys_t last_path;
	  event::key_t last_key;
	  using temp_map_t = std::map<event::keys_t, value_t>;
	  temp_map_t parents;
	  using value_store_t = std::vector<value_t>;
	  value_store_t value_store;
	  event::keys_t prev_path_arr;

     private:
	  void clear();
	  Mode mode;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnf_Builder_hpp__
