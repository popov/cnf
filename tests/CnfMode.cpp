#include <Mode.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(Mode, test_01)
{
     Mode M;
     //ASSERT_EQ(0, dt.size());
}

TEST(Mode, test_02)
{
     Mode M;
     ASSERT_TRUE(M.table());
     ASSERT_FALSE(M.array());
     ASSERT_FALSE(M.inlin());
}

TEST(Mode, test_03)
{
     Mode M;
     M.to_inline();
     ASSERT_TRUE(M.inlin());
     M.to_array();
     ASSERT_TRUE(M.array());
     M.to_array();
     ASSERT_TRUE(M.array());
     M.release();
     ASSERT_TRUE(M.array());
     M.release();
     ASSERT_TRUE(M.inlin());
     M.release();
     ASSERT_TRUE(M.table());
}
