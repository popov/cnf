#include <TableBuilder.hpp>
#include <Stringer.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(TableBuilder, test_01)
{
     dict_t result;
     parents_t parents;
     path_t p = { "ab1", "ac2", "ad3" };
     TableBuilder TB(result, parents);
     TB.build(p);
     ASSERT_EQ(1, result.size());
}

TEST(TableBuilder, test_02)
{
     dict_t result;
     parents_t parents;
     path_t p = { "ab1", "ac2", "ad3" };
     TableBuilder TB(result, parents);
     TB.build(p);
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(result, ss);
     std::cerr << ss.str();
     ASSERT_EQ(1, result.count("ab1"));
     auto r2 = (Dict*)((result.at("ab1").get()));
     ASSERT_EQ(1, r2->content.count("ac2"));
     auto r3 = (Dict*)(r2->content.at("ac2").get());
     ASSERT_EQ(1, r3->content.count("ad3"));
}

TEST(TableBuilder, test_03)
{
     dict_t result;
     parents_t parents;
     path_t p1 = { "ab1" };
     TableBuilder TB(result, parents);
     TB.build(p1);
     path_t p2 = { "XYZ" };
     TB.build(p2);
     path_t p3 = { "ab1", "ac2", "ad3" };
     TB.build(p3);
     ASSERT_EQ(2, result.size());
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(result, ss);
     std::cerr << ss.str();
     ASSERT_EQ(1, result.count("ab1"));
     auto r2 = (Dict*)((result.at("ab1").get()));
     ASSERT_EQ(1, r2->content.count("ac2"));
     auto r3 = (Dict*)(r2->content.at("ac2").get());
     ASSERT_EQ(1, r3->content.count("ad3"));
}
