#include <Stringer.hpp>
#include <Handler.hpp>
#include <Builder.hpp>
#include <gtest/gtest.h>
#include <iostream>

using namespace cnf;

template <class T>
void print(const T& s) {
     std::cerr << std::endl;
     std::cerr << "*** print: " << s;
     std::cerr << std::endl;
}

     std::string input(R"foo(
################################################################################
## Comment

# Speak your mind with the hash symbol. They go from the symbol to the end of
# the line.

key1 = 1323
228 = 228

################################################################################
## Table

# Tables (also known as hash tables or dictionaries) are collections of
# key/value pairs. They appear in square brackets on a line by themselves.

[table]

key = "value" # Yeah, you can do this.

# Nested tables are denoted by table names with dots in them. Name your tables
# whatever crap you please, just don't use #, ., [ or ].

[table.subtable]

key = "another value"

# You don't need to specify all the super-tables if you don't want to. TOML
# knows how to do it for you.

# [x] you
# [x.y] don't
# [x.y.z] need these
[x.y.z.w] # for this to work


################################################################################
## Inline Table

# Inline tables provide a more compact syntax for expressing tables. They are
# especially useful for grouped data that can otherwise quickly become verbose.
# Inline tables are enclosed in curly braces `{` and `}`. No newlines are
# allowed between the curly braces unless they are valid within a value.

[table.inline]

name = { first = "Tom", last = "Preston-Werner" }
point = { x = 1, y = 2 }


################################################################################
## String

# There are four ways to express strings: basic, multi-line basic, literal, and
# multi-line literal. All strings must contain only valid UTF-8 characters.

[string.basic]

basic = "I'm a string. \"You can quote me\". Name\tJos\u00E9\nLocation\tSF."

[string.multiline]

# The following strings are byte-for-byte equivalent:
key1 = "One\nTwo"
key2 = """One\nTwo"""
key3 = """
One
Two"""

[string.multiline.continued]

# The following strings are byte-for-byte equivalent:
key1 = "The quick brown fox jumps over the lazy dog."

key2 = """
The quick brown \


  fox jumps over \
    the lazy dog."""

key3 = """\
       The quick brown \
       fox jumps over \
       the lazy dog.\
       """

[string.literal]

# What you see is what you get.
winpath  = 'C:\Users\nodejs\templates'
winpath2 = '\\ServerX\admin$\system32\'
quoted   = 'Tom "Dubs" Preston-Werner'
regex    = '<\i\c*\s*>'


[string.literal.multiline]

regex2 = '''I [dw]on't need \d{2} apples'''
lines  = '''
The first newline is
trimmed in raw strings.
   All other whitespace
   is preserved.
'''


################################################################################
## Integer

# Integers are whole numbers. Positive numbers may be prefixed with a plus sign.
# Negative numbers are prefixed with a minus sign.

[integer]

key1 = +99
key2 = 42
key3 = 0
key4 = -17

[integer.underscores]

# For large numbers, you may use underscores to enhance readability. Each
# underscore must be surrounded by at least one digit.
key1 = 1_000
key2 = 5_349_221
key3 = 1_2_3_4_5     # valid but inadvisable


################################################################################
## Float

# A float consists of an integer part (which may be prefixed with a plus or
# minus sign) followed by a fractional part and/or an exponent part.

[float.fractional]

key1 = +1.0
key2 = 3.1415
key3 = -0.01

[float.exponent]

key1 = 5e+22
key2 = 1e6
key3 = -2E-2

[float.both]

key = 6.626e-34

[float.underscores]

key1 = 9_224_617.445_991_228_313
key2 = 1e1_00


################################################################################
## Boolean

# Booleans are just the tokens you're used to. Always lowercase.

[boolean]

True = true
False = false


################################################################################
## Datetime

# Datetimes are RFC 3339 dates.

[datetime]

key1 = 1979-05-27T07:32:00Z
key2 = 1979-05-27T00:32:00-07:00
key3 = 1979-05-27T00:32:00.999999-07:00


################################################################################
## Array

# Arrays are square brackets with other primitives inside. Whitespace is
# ignored. Elements are separated by commas. Data types may not be mixed.

[array]

key1 = [ 1, 2, 3 ]
key2 = [ "red", "yellow", "green" ]
key3 = [ [ 1, 2 ], [3, 4, 5] ]
key4 = [ [ 1, 2 ], ["a", "b", "c"] ] # this is ok

# Arrays can also be multiline. So in addition to ignoring whitespace, arrays
# also ignore newlines between the brackets.  Terminating commas are ok before
# the closing bracket.

key5 = [
  1, 2, 3
]
key6 = [
  1,
  2, # this is ok
]


################################################################################
## Array of Tables

# These can be expressed by using a table name in double brackets. Each table
# with the same double bracketed name will be an element in the array. The
# tables are inserted in the order encountered.

[[products]]

name = "Hammer"
sku = 738594937

[[products]]

[[products]]

name = "Nail"
sku = 284758393
color = "gray"


# You can create nested arrays of tables as well.

[[fruit]]
  name = "apple"

  [fruit.physical]
    color = "red"
    shape = "round"

  [[fruit.variety]]
    name = "red delicious"

  [[fruit.variety]]
    name = "granny smith"

[[fruit]]
  name = "banana"

  [[fruit.variety]]
    name = "plantain"
)foo");

TEST(CnfStringer, test_01)
{
     std::stringstream ss(input);
     event::events_t evs;
     Handler H(evs, ss);
     auto st = H.parse();
     ASSERT_EQ(Handler::OK, st.state);
}

TEST(CnfStringer, test_02)
{
     std::stringstream ss(input);
     event::events_t evs;
     Handler H(evs, ss);
     auto st = H.parse();
     ASSERT_NE(0, evs.size());
     print(evs.size());
}

TEST(CnfStringer, test_03)
{
     std::stringstream ss(input);
     event::events_t evs;
     Handler H(evs, ss);
     auto st = H.parse();
     dict_t d;
     Builder B(d, evs);
     B.build();
     ASSERT_NE(0, d.size());
     print(d.size());
}

std::string input_2(R"foo(

228 = 228
key1 = 1323

[[fruit]]
name = "apple"

[[fruit.variety]]
name = "red delicious"

[[fruit.variety]]
name = "granny smith"

[fruit.physical]
color = "red"
shape = "round"

[[fruit]]
name = "banana"

[[fruit.variety]]
name = "plantain"

[[products]]
name = "Hammer"
sku = 738594937

[[products]]

[[products]]
color = "gray"
name = "Nail"
sku = 284758393

[array]

key1 = [
  1,
  2,
  3,
]

key2 = [
  "red",
  "yellow",
  "green",
]

key3 = [ [
  1,
  2,
], [
  3,
  4,
  5,
], ]

key4 = [ [
  1,
  2,
], [
  "a",
  "b",
  "c",
], ]

key5 = [
  1,
  2,
  3,
]

key6 = [
  1,
  2,
]

[boolean]
False = false
True = true

[datetime]
key1 = "1979-05-27T07:32:00Z"
key2 = "1979-05-27T00:32:00-07:00"
key3 = "1979-05-27T00:32:00.999999-07:00"

[float.both]
key = 0.000000

[float.fractional]
key1 = 1.000000
key2 = 3.141500
key3 = -0.010000

[integer]
key1 = 99
key2 = 42
key3 = 0
key4 = -17

[table]
key = "value"

[table.inline.name]
first = "Tom"
last = "Preston-Werner"

[table.inline.point]
x = 1
y = 2

[table.subtable]
key = "another value"


)foo");

TEST(CnfStringer, test_31)
{
     std::stringstream ss(input_2);
     event::events_t evs;
     Handler H(evs, ss);
     auto st = H.parse();
     ASSERT_EQ(Handler::OK, st.state);
     dict_t d;
     Builder B(d, evs);
     B.build();

     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     std::stringstream res;
     DS.fill(d, res);
     print(d.size());
     //print(res.str());
     //ASSERT_EQ(etalon_06, res.str());
     //ASSERT_NE(0, d.size());
     //print(d.size());
}

     std::string input_04(R"foo(
## Comment

# Speak your mind with the hash symbol. They go from the symbol to the end of
# the line.

key1 = 1323
228 = 228

################################################################################
## Table

# Tables (also known as hash tables or dictionaries) are collections of
# key/value pairs. They appear in square brackets on a line by themselves.

[table]

key = "value" # Yeah, you can do this.

# Nested tables are denoted by table names with dots in them. Name your tables
# whatever crap you please, just don't use #, ., [ or ].

[table.subtable]

key = "another value"

[x]
key1 = 1323

)foo");

std::string etalon_04(R"foo(228 = 228
key1 = 1323

[table]
key = "value"

[table.subtable]
key = "another value"

[x]
key1 = 1323
)foo");
TEST(CnfStringer, test_04)
{
     std::stringstream ss(input_04);
     event::events_t evs;
     Handler H(evs, ss);
     auto st = H.parse();
     dict_t d;
     Builder B(d, evs);
     B.build();

     ASSERT_EQ(4, d.size());

     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     std::stringstream res;
     DS.fill(d, res);
     //print(res.str());
     ASSERT_EQ(etalon_04, res.str());
}

std::string input_05(R"foo(
[[fruit]]
  name = "apple"

  [fruit.physical]
    color = "red"
    shape = "round"

  [[fruit.variety]]
    name = "red delicious"

  [[fruit.variety]]
    name = "granny smith"

[[fruit]]
  name = "banana"

  [[fruit.variety]]
    name = "plantain"
)foo");
std::string etalon_05(R"foo(
[[fruit]]
name = "apple"

[[fruit.variety]]
name = "red delicious"

[[fruit.variety]]
name = "granny smith"

[fruit.physical]
color = "red"
shape = "round"

[[fruit]]
name = "banana"

[[fruit.variety]]
name = "plantain"
)foo");
TEST(CnfStringer, test_05)
{
     std::stringstream ss(input_05);
     event::events_t evs;
     Handler H(evs, ss);
     auto st = H.parse();
     dict_t d;
     Builder B(d, evs);
     B.build();

     ASSERT_EQ(1, d.size());

     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     std::stringstream res;
     DS.fill(d, res);
     ASSERT_EQ(etalon_05, res.str());
     //print(res.str());
}

std::string input_06(R"foo(
[[a]]
index = 1

[[a.b]]
index = 3

[[a]]
index = 2

[[a.b]]
index = 4

)foo");
std::string etalon_06(R"foo(
[[a]]
index = 1

[[a.b]]
index = 3

[[a]]
index = 2

[[a.b]]
index = 4
)foo");
TEST(CnfStringer, test_06)
{
     std::stringstream ss(input_06);
     event::events_t evs;
     Handler H(evs, ss);
     auto st = H.parse();
     ASSERT_EQ(Handler::OK, st.state);
     dict_t d;
     Builder B(d, evs);
     B.build();

     ASSERT_EQ(1, d.size());

     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     std::stringstream res;
     DS.fill(d, res);
     ASSERT_EQ(etalon_06, res.str());
     //print(res.str());
}
