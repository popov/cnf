#include <ScalarLinker.hpp>
#include <TableBuilder.hpp>
#include <TablesArrayBuilder.hpp>
#include <Stringer.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(ScalarLinker, test_01)
{
     dict_t result;
     parents_t parents;
     //path_t p = { "ab1", "ac2", "ad3" };
     path_t p;
     ScalarLinker SL(result, parents);
     auto v = get<value_t>(Int(99));
     values_t vs = { v };
     SL.link(p, "key", vs);
     ASSERT_EQ(0, vs.size());
     ASSERT_EQ(1, result.size());

     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(result, ss);
     std::cerr << ss.str();
}

TEST(ScalarLinker, test_02)
{
     dict_t result;
     parents_t parents;
     path_t p = { "ab1" };
     //path_t p;

     TableBuilder TB(result, parents);
     TB.build(p);
     
     ScalarLinker SL(result, parents);
     auto v = get<value_t>(Int(101));
     values_t vs = { v };
     SL.link(p, "key", vs);
     ASSERT_EQ(0, vs.size());
     ASSERT_EQ(1, result.size());
     auto r2 = (Dict*)((result.at("ab1").get()));
     ASSERT_EQ(1, r2->content.size());
     
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(result, ss);
     std::cerr << ss.str();
}

TEST(ScalarLinker, test_03)
{
     dict_t result;
     parents_t parents;
     path_t p = { "ab1" };
     path_t px;

     TablesArrayBuilder TB(result, parents, px);
     TB.build(p);
     
     ScalarLinker SL(result, parents);
     auto v = get<value_t>(Int(303));
     values_t vs = { v };
     SL.link(p, "key", vs);
     ASSERT_EQ(0, vs.size());
     ASSERT_EQ(1, result.size());

     auto r = result.at("ab1");
     ASSERT_EQ(Value::ARRAY, r->type);
     Array* ar = (Array*)(r.get());
     ASSERT_EQ(Value::DICT, ar->a_type);
     ASSERT_EQ(1, ar->content.size());

     auto tt = ar->content.at(0);
     auto ttt = (Dict*)(tt.get());
     ASSERT_EQ(1, ttt->content.size());

     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(result, ss);
     std::cerr << ss.str();
}
