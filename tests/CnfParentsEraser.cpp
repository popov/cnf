#include <ParentsEraser.hpp>
#include <gtest/gtest.h>

using namespace cnf;

using path = std::vector<int>;
using parents = std::map<path, int>;

TEST(ParentsEraser, test_01)
{
     path p;
     parents v;
     ParentsEraser<int, int> PE(p, v);
}

TEST(ParentsEraser, test_02)
{
     path p = { 1, 2, 3 };
     parents v = {
	  { { 1, 2, 3 },    1 },
	  { { 1, 2, 3, 4 }, 2 },
     };
     ParentsEraser<int, int> PE(p, v);
     PE.erase();
     ASSERT_EQ(1, v.size());
}

TEST(ParentsEraser, test_03)
{
     path p = { 1, 2, 3 };
     parents v = {
	  { { 1, 2 },       0 },
	  { { 1, 2, 3 },    1 },
	  { { 1, 2, 3, 4 }, 2 },
     };
     ParentsEraser<int, int> PE(p, v);
     PE.erase();
     ASSERT_EQ(2, v.size());
}

TEST(ParentsEraser, test_04)
{
     path p = { 1, 2, 3 };
     parents v = {
	  { { 1, 2 },       0 },
	  { { 1, 2, 3 },    1 },
	  { { 1, 2, 5, 4 }, 2 },
     };
     ParentsEraser<int, int> PE(p, v);
     PE.erase();
     ASSERT_EQ(3, v.size());
}

TEST(ParentsEraser, test_05)
{
     path p = { 1, 2, 3 };
     parents v = {
	  { { 1, 2 },       0 },
	  { { 1, 2, 3 },    1 },
	  { { 1, 2, 3, 4 }, 2 },
	  { { 1, 2, 3, 4, 5 }, 3 },
     };
     ParentsEraser<int, int> PE(p, v);
     PE.erase();
     ASSERT_EQ(2, v.size());
}
