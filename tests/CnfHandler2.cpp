#include <Handler.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(Handler2, test_01)
{
     std::string i(R"foo(


#[[products]]
#
#name = "Hammer"
#sku = 738594937
#
#[[products]]
#
#[[products]]
#
#name = "Nail"
#sku = 284758393
#color = "gray"


[[fruit]]
  name = "apple"

  [fruit.physical] # вложенность по имени 
    color = "red"
    shape = "round"

  [[fruit.variety]]
    name = "red delicious"

  [[fruit.variety]]
    name = "granny smith"

[[fruit]]
  name = "banana"

  [[fruit.variety]]
    name = "plantain"




)foo");
     std::string etalon(R"foo(00 START
01 COMMENT: [[products]]
02 COMMENT: 
03 COMMENT: name = "Hammer"
04 COMMENT: sku = 738594937
05 COMMENT: 
06 COMMENT: [[products]]
07 COMMENT: 
08 COMMENT: [[products]]
09 COMMENT: 
10 COMMENT: name = "Nail"
11 COMMENT: sku = 284758393
12 COMMENT: color = "gray"
13 TABLES_ARRAY: fruit
14 KEY: name
15 STRING: apple
16 TABLE: fruit/physical
17 COMMENT:  вложенность по имени 
18 KEY: color
19 STRING: red
20 KEY: shape
21 STRING: round
22 TABLES_ARRAY: fruit/variety
23 KEY: name
24 STRING: red delicious
25 TABLES_ARRAY: fruit/variety
26 KEY: name
27 STRING: granny smith
28 TABLES_ARRAY: fruit
29 KEY: name
30 STRING: banana
31 TABLES_ARRAY: fruit/variety
32 KEY: name
33 STRING: plantain
34 FINISH
)foo");

     std::stringstream input(i);
     event::events_t evs;
     Handler H2(evs, input);
     auto status = H2.parse();
     ASSERT_EQ(Handler::OK, status.state);
     ASSERT_EQ("", status.msg);
     //std::cerr << event::dump(evs);
     ASSERT_EQ(etalon, event::dump(evs));
}

TEST(Handler2, test_02)
{
     std::string i(R"foo(

[[products]]
#
name = "Hammer"
sku = 738594937
#
[[products]]
#
[[products]]
#
name = "Nail"
sku = 284758393
color = "gray"

)foo");
     std::string etalon(R"foo(
TODO
)foo");

     std::stringstream input(i);
     event::events_t evs;
     Handler H2(evs, input);
     auto status = H2.parse();
     ASSERT_EQ(Handler::OK, status.state);
     ASSERT_EQ("", status.msg);
     ASSERT_EQ(19, evs.size());
}

TEST(Handler2, test_03)
{
     std::string i(R"foo(

[[products]]
#
name = "Hammer"
sku = 738594937
#
[[products]]
#
[[products]]
#
name = "Nail"
sku = 284758393
color = "gray"

)foo");
     std::string etalon(R"foo(00 START
01 TABLES_ARRAY: products
02 COMMENT: 
03 KEY: name
04 STRING: Hammer
05 KEY: sku
06 INTEGER: 738594937
07 COMMENT: 
08 TABLES_ARRAY: products
09 COMMENT: 
10 TABLES_ARRAY: products
11 COMMENT: 
12 KEY: name
13 STRING: Nail
14 KEY: sku
15 INTEGER: 284758393
16 KEY: color
17 STRING: gray
18 FINISH
)foo");

     std::stringstream input(i);
     event::events_t evs;
     Handler H2(evs, input);
     auto status = H2.parse();
     ASSERT_EQ(Handler::OK, status.state);
     ASSERT_EQ("", status.msg);
     ASSERT_EQ(19, evs.size());
     ASSERT_EQ(etalon, event::dump(evs));
     //std::cerr << event::dump(evs);
}
