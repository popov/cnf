#include <cnf/Configuration.hpp>
#include <cnf/LayerCommon.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(Configuration, test_01)
{
     auto& c1 = Configuration::get();
     auto& c2 = Configuration::get();
     auto& c3 = Configuration::get();
}

TEST(Configuration, test_02)
{
     auto& c = Configuration::get();
     ASSERT_EQ(0, c.layer().dict.size());
}

TEST(Configuration, test_03)
{
     auto& c = Configuration::get();
     auto& l = c.layer().dict;
     l["x"] = get<value_t>(Int(1));
     ASSERT_EQ(1, c.layer().dict.size());
}

TEST(Configuration, test_04)
{
     auto& c = Configuration::get();
     ASSERT_EQ(1, c.layer().dict.size());
}

std::string etalon_05(R"foo(x = 1
)foo");
TEST(Configuration, test_05)
{
     auto& c = Configuration::get();
     auto& l = c.layer();
     std::stringstream ss;
     str(l, ss);
     //std::cerr << ss.str();
     ASSERT_EQ(etalon_05, ss.str());
}

std::string etalon_06(R"foo(name_2 = "two"
x = 2

[a]
x = 3

[a.b]
x = 4

[a.b.c]
scan_2 = "two"
x = 2
)foo");
std::string input_06(R"foo(
name_2 = "two"
x = 2

[a]
x = 3

[a.b]
x = 4

[a.b.c]
scan_2 = "two"
x = 2
)foo");
TEST(Configuration, test_06)
{
     std::stringstream si(input_06);
     Layer l2(2);
     build(si, l2);
     auto& c = Configuration::get();
     c << l2;
     auto& l = c.layer();
     std::stringstream ss;
     str(l, ss);
     //std::cerr << ss.str();
     ASSERT_EQ(etalon_06, ss.str());
}
