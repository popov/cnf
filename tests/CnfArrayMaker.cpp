#include <ArrayMaker.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(ArrayMaker, test_01)
{
     auto a1 = get<value_t>(Int(1));
     auto a2 = get<value_t>(Int(2));
     auto a3 = get<value_t>(Int(3));
     values_t vs = { a1, a2, a3 };
     ArrayMaker::make(vs, 3);
     ASSERT_EQ(1, vs.size());
     auto a = vs.at(0);
     ASSERT_EQ(Value::ARRAY, a->type);
     Array* AR = (Array*)(a.get());
     ASSERT_EQ(3, AR->content.size());
     ASSERT_EQ(Value::INT, AR->a_type);
     Value* v1 = AR->content.at(0).get();
     Value* v2 = AR->content.at(1).get();
     Value* v3 = AR->content.at(2).get();
     ASSERT_EQ(1, ((Int*)(v1))->content);
     ASSERT_EQ(2, ((Int*)(v2))->content);
     ASSERT_EQ(3, ((Int*)(v3))->content);
}

TEST(ArrayMaker, test_02)
{
     auto a0 = get<value_t>(Float(1.5));
     auto a1 = get<value_t>(Int(1));
     auto a2 = get<value_t>(Int(2));
     auto a3 = get<value_t>(Int(3));
     values_t vs = { a0, a1, a2, a3 };
     ArrayMaker::make(vs, 3);
     ASSERT_EQ(2, vs.size());
     auto a = vs.at(1);
     ASSERT_EQ(Value::ARRAY, a->type);
     Array* AR = (Array*)(a.get());
     ASSERT_EQ(3, AR->content.size());
     ASSERT_EQ(Value::INT, AR->a_type);
     Value* v1 = AR->content.at(0).get();
     Value* v2 = AR->content.at(1).get();
     Value* v3 = AR->content.at(2).get();
     ASSERT_EQ(1, ((Int*)(v1))->content);
     ASSERT_EQ(2, ((Int*)(v2))->content);
     ASSERT_EQ(3, ((Int*)(v3))->content);
}

TEST(ArrayMaker, test_03)
{
     auto a1 = get<value_t>(Int(1));
     auto a2 = get<value_t>(Int(2));
     auto a3 = get<value_t>(Int(3));
     values_t vs = { a1, a2, a3 };
     ArrayMaker::make(vs, 5);
     ASSERT_EQ(1, vs.size());
     auto a = vs.at(0);
     ASSERT_EQ(Value::ARRAY, a->type);
     Array* AR = (Array*)(a.get());
     ASSERT_EQ(3, AR->content.size());
     ASSERT_EQ(Value::INT, AR->a_type);
     Value* v1 = AR->content.at(0).get();
     Value* v2 = AR->content.at(1).get();
     Value* v3 = AR->content.at(2).get();
     ASSERT_EQ(1, ((Int*)(v1))->content);
     ASSERT_EQ(2, ((Int*)(v2))->content);
     ASSERT_EQ(3, ((Int*)(v3))->content);
}
