#include <TablesArrayBuilder.hpp>
#include <TableBuilder.hpp>
#include <Stringer.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(TablesArrayBuilder, test_01)
{
     dict_t result;
     parents_t parents;
     path_t p = { "ab1", "ac2", "ad3" };
     path_t p2;
     TablesArrayBuilder TB(result, parents, p2);
     TB.build(p);
     ASSERT_EQ(1, result.size());
}

TEST(TablesArrayBuilder, test_02)
{
     dict_t result;
     parents_t parents;
     path_t p1 = { "ab1", "ac2", "ad3" };
     path_t p2 = { "xab1" };
     TablesArrayBuilder TB(result, parents, p2);
     TB.build(p1);
     TB.build(p2);
     ASSERT_EQ(2, result.size());
}

TEST(TablesArrayBuilder, test_03)
{
     dict_t result;
     parents_t parents;
     path_t p = { "ab1", "ac2", "ad3" };
     path_t p2;
     TablesArrayBuilder TB(result, parents, p2);
     TB.build(p);
     auto r = result.at("ab1");
     ASSERT_EQ(Value::DICT, r->type);
     auto r2 = result.at("ab1");
     ASSERT_EQ(Value::DICT, r2->type);
     auto r3 = ((Dict*)(r2.get()))->content.at("ac2");
     ASSERT_EQ(Value::DICT, r3->type);
     auto r4 = ((Dict*)(r3.get()))->content.at("ad3");
     ASSERT_EQ(Value::ARRAY, r4->type);
     Array* ar = (Array*)(r4.get());
     ASSERT_EQ(Value::DICT, ar->a_type);
     ASSERT_EQ(1, ar->content.size());
}

TEST(TablesArrayBuilder, test_04)
{
     dict_t result;
     parents_t parents;
     path_t p = { "ab1" };
     path_t p2;
     TablesArrayBuilder TB(result, parents, p2);
     TB.build(p);
     auto r = result.at("ab1");
     ASSERT_EQ(Value::ARRAY, r->type);
     Array* ar = (Array*)(r.get());
     ASSERT_EQ(Value::DICT, ar->a_type);
     ASSERT_EQ(1, ar->content.size());
}

TEST(TablesArrayBuilder, test_05)
{
     dict_t result;
     parents_t parents;
     path_t p = { "ab1" };
     path_t px;
     TablesArrayBuilder TB(result, parents, px);
     TB.build(p);

     TableBuilder TB2(result, parents);
     path_t p2 = { "ab1", "xxx" };
     TB2.build(p2);
     
     auto r = result.at("ab1");
     ASSERT_EQ(Value::ARRAY, r->type);
     Array* ar = (Array*)(r.get());
     ASSERT_EQ(Value::DICT, ar->a_type);
     ASSERT_EQ(1, ar->content.size());

     auto aaa = ar->content.at(0);
     ASSERT_EQ(Value::DICT, aaa->type);
}

TEST(TablesArrayBuilder, test_06)
{
     dict_t result;
     parents_t parents;
     path_t p = { "ab1" };
     path_t px = { "WER" };
     TablesArrayBuilder TB(result, parents, px);
     TB.build(p);

     TableBuilder TB2(result, parents);
     path_t p2 = { "ab1", "xxx" };
     TB2.build(p2);

     TB.build(p);
     
     auto r = result.at("ab1");
     ASSERT_EQ(Value::ARRAY, r->type);
     Array* ar = (Array*)(r.get());
     ASSERT_EQ(Value::DICT, ar->a_type);
     ASSERT_EQ(2, ar->content.size());

     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::DUMP);
     DS.fill(result, ss);
     std::cerr << ss.str();
}
