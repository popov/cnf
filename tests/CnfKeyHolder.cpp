#include <KeyHolder.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(KeyHolder, test_01)
{
     KeyHolder kh;
     ASSERT_TRUE(kh.empty());
}

TEST(KeyHolder, test_02)
{
     KeyHolder kh;
     kh.push("one");
     ASSERT_FALSE(kh.empty());
}

TEST(KeyHolder, test_03)
{
     KeyHolder kh;
     kh.push("one");
     kh.push("two");
     kh.push("three");
     kh.release();
     ASSERT_FALSE(kh.empty());
}

TEST(KeyHolder, test_04)
{
     KeyHolder kh;
     kh.release();
     ASSERT_TRUE(kh.empty());
}

TEST(KeyHolder, test_05)
{
     KeyHolder kh;
     kh.push("one");
     kh.push("two");
     kh.push("three");
     ASSERT_EQ("one.two.three", kh.str());
     kh.release();
     ASSERT_EQ("one.two", kh.str());
     kh.release();
     ASSERT_EQ("one", kh.str());
     kh.release();
     ASSERT_EQ("", kh.str());
}

TEST(KeyHolder, test_06)
{
     KeyHolder kh;
     kh.push("one");
     kh.push("two");
     kh.push("three");
     ASSERT_EQ("one.two.three", kh.str());
     KeyHolder kh2(kh);
     ASSERT_EQ("one.two.three", kh2.str());
}

TEST(KeyHolder, test_07)
{
     KeyHolder kh;
     kh.push("a");
     kh.push("b");
     kh.push("c");
     kh.push("d.e.f");
     ASSERT_EQ("a.b.c.\"d.e.f\"", kh.str());
}
