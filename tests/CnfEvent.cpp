#include <Event.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(CnfEvent, test_01)
{
     event::Start s;
     ASSERT_EQ(s.type, event::START);
}

TEST(CnfEvent, test_02)
{
     event::Finish s;
     ASSERT_EQ(s.type, event::FINISH);
}

TEST(CnfEvent, test_03)
{
     event::Comment s("comment");
     ASSERT_EQ(s.type, event::COMMENT);
}

TEST(CnfEvent, test_04)
{
     event::keys_t k{{"one", "two", "three"}};
}

TEST(CnfEvent, test_05)
{
     event::keys_t k{{"one", "two", "three"}};
     event::TablesArray s(k);
     ASSERT_EQ(s.type, event::TABLES_ARRAY);
}

TEST(CnfEvent, test_06)
{
     event::keys_t k{{"one", "two", "three"}};
     event::Table s(k);
     ASSERT_EQ(s.type, event::TABLE);
}

TEST(CnfEvent, test_07)
{
     event::Key s("key");
     ASSERT_EQ(s.type, event::KEY);
}

TEST(CnfEvent, test_08)
{
     event::StartInline s;
     ASSERT_EQ(s.type, event::START_INLINE);
}

TEST(CnfEvent, test_09)
{
     event::FinishInline s(4);
     ASSERT_EQ(s.type, event::FINISH_INLINE);
}

TEST(CnfEvent, test_10)
{
     event::StartArray s;
     ASSERT_EQ(s.type, event::START_ARRAY);
}

TEST(CnfEvent, test_11)
{
     event::FinishArray s(4);
     ASSERT_EQ(s.type, event::FINISH_ARRAY);
}

TEST(CnfEvent, test_12)
{
     event::String s("4");
     ASSERT_EQ(s.type, event::STRING);
}

TEST(CnfEvent, test_13)
{
     event::Boolean s(true);
     ASSERT_EQ(s.type, event::BOOLEAN);
}

TEST(CnfEvent, test_14)
{
     event::DateTime s("4");
     ASSERT_EQ(s.type, event::DATE_TIME);
}

TEST(CnfEvent, test_15)
{
     event::Integer s(4);
     ASSERT_EQ(s.type, event::INTEGER);
}

TEST(CnfEvent, test_16)
{
     event::Float s(4.5);
     ASSERT_EQ(s.type, event::FLOAT);
}
