#include <cnf/LayerCommon.hpp>
#include <gtest/gtest.h>
#include <sstream>

using namespace cnf;

TEST(LayerCommon, test_01)
{
     Layer l(1);
     std::stringstream ss;
     dump(l, ss);
     std::cerr << ss.str();
}

TEST(LayerCommon, test_02)
{
     Layer l(1);
     std::stringstream ss;
     str(l, ss);
     std::cerr << ss.str();
}

std::string input_03(R"foo(
[x.y.z]
key = 1
)foo");
std::string etalon_03(R"foo(
[x]

[x.y]

[x.y.z]
key = 1
)foo");
TEST(LayerCommon, test_03)
{
     Layer l(1);
     std::stringstream si(input_03);
     ASSERT_TRUE(build(si, l));
     std::stringstream so;
     dump(l, so);
     ASSERT_EQ(etalon_03, so.str());
     //std::cerr << so.str();
}

std::string input_04_1(R"foo(
x = 6
)foo");
std::string input_04_2(R"foo(
y = 7
)foo");
std::string etalon_04(R"foo(x = 6
y = 7
)foo");
TEST(LayerCommon, test_04)
{
     std::stringstream si_1(input_04_1);
     std::stringstream si_2(input_04_2);
     Layer l1(1);
     Layer l2(2);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_04, so.str());
     //std::cerr << so.str();
}

std::string input_05_1(R"foo(
x = 6
[one]
x_one = 1
)foo");
std::string input_05_2(R"foo(
y = 7
[two]
x_one = 1
)foo");
std::string etalon_05(R"foo(x = 6
y = 7

[one]
x_one = 1

[two]
x_one = 1
)foo");
TEST(LayerCommon, test_05)
{
     std::stringstream si_1(input_05_1);
     std::stringstream si_2(input_05_2);
     Layer l1(1);
     Layer l2(2);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_05, so.str());
     //std::cerr << so.str();
}

std::string input_06_1(R"foo(
y = 6.6
)foo");
std::string input_06_2(R"foo(
y = 7
)foo");
std::string etalon_06(R"foo(y = 7
)foo");
TEST(LayerCommon, test_06)
{
     std::stringstream si_1(input_06_1);
     std::stringstream si_2(input_06_2);
     Layer l1(1);
     Layer l2(2);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_06, so.str());
     //std::cerr << so.str();
}

std::string input_07_1(R"foo(
y = "one"
)foo");
std::string input_07_2(R"foo(
y = 7
)foo");
std::string etalon_07(R"foo(y = "one"
)foo");
TEST(LayerCommon, test_07)
{
     std::stringstream si_1(input_07_1);
     std::stringstream si_2(input_07_2);
     Layer l1(1);
     Layer l2(1);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_07, so.str());
     //std::cerr << so.str();
}

std::string input_08_1(R"foo(
y = [ 1, 2, 3 ]
)foo");
std::string input_08_2(R"foo(
y = [ 4, 5, 6, 7 ]
)foo");
std::string etalon_08(R"foo(
y = [
  4,
  5,
  6,
  7,
]
)foo");
TEST(LayerCommon, test_08)
{
     std::stringstream si_1(input_08_1);
     std::stringstream si_2(input_08_2);
     Layer l1(1);
     Layer l2(2);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_08, so.str());
     //std::cerr << so.str();
}

std::string input_09_1(R"foo(
y = [ 1, 2, 3 ]
)foo");
std::string input_09_2(R"foo(
y = [ 4, 5, 6, 7 ]
)foo");
std::string etalon_09(R"foo(
y = [
  1,
  2,
  3,
]
)foo");
TEST(LayerCommon, test_09)
{
     std::stringstream si_1(input_09_1);
     std::stringstream si_2(input_09_2);
     Layer l1(1);
     Layer l2(1);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_09, so.str());
     //std::cerr << so.str();
}

std::string input_10_1(R"foo(
name_1 = "one"
x = 1

[a.b.c]
scan_1 = "one"
x = 1
y = 1
z = 1
)foo");
std::string input_10_2(R"foo(
name_2 = "two"
x = 1

[a.b.c]
scan_2 = "two"
x = 4
y = 5
z = 6
)foo");
std::string etalon_10(R"foo(name_1 = "one"
name_2 = "two"
x = 1

[a.b.c]
scan_1 = "one"
scan_2 = "two"
x = 4
y = 5
z = 6
)foo");
TEST(LayerCommon, test_10)
{
     std::stringstream si_1(input_10_1);
     std::stringstream si_2(input_10_2);
     Layer l1(1);
     Layer l2(2);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_10, so.str());
     //std::cerr << so.str();
}

std::string input_11_1(R"foo(
name_1 = "one"
x = 1

[a.b.c]
scan_1 = "one"
x = 1
y = 1
z = 1
)foo");
std::string input_11_2(R"foo(
name_2 = "two"
x = 1

[a.b.c]
scan_2 = "two"
x = 4
y = 5
z = 6
)foo");
std::string etalon_11(R"foo(name_1 = "one"
name_2 = "two"
x = 1

[a.b.c]
scan_1 = "one"
scan_2 = "two"
x = 1
y = 1
z = 1
)foo");
TEST(LayerCommon, test_11)
{
     std::stringstream si_1(input_11_1);
     std::stringstream si_2(input_11_2);
     Layer l1(1);
     Layer l2(1);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_11, so.str());
     //std::cerr << so.str();
}

std::string input_12_1(R"foo(
name_1 = "one"
x = 1

[a.b.c]
scan_1 = "one"
x = 1
)foo");
std::string input_12_2(R"foo(
name_2 = "two"
x = 2

[a]
x = 3

[a.b]
x = 4

[a.b.c]
scan_2 = "two"
x = 2
)foo");
std::string etalon_12(R"foo(name_1 = "one"
name_2 = "two"
x = 1

[a]
x = 3

[a.b]
x = 4

[a.b.c]
scan_1 = "one"
scan_2 = "two"
x = 1
)foo");
TEST(LayerCommon, test_12)
{
     std::stringstream si_1(input_12_1);
     std::stringstream si_2(input_12_2);
     Layer l1(1);
     Layer l2(1);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_12, so.str());
     //std::cerr << so.str();
}
std::string input_13_1(R"foo(
name_1 = "one"
x = 1

[a.b.c]
scan_1 = "one"
x = 1
)foo");
std::string input_13_2(R"foo(
name_2 = "two"
x = 2

[a]
x = 3

[a.b]
x = 4

[a.b.c]
scan_2 = "two"
x = 2
)foo");
std::string etalon_13(R"foo(name_1 = "one"
name_2 = "two"
x = 2

[a]
x = 3

[a.b]
x = 4

[a.b.c]
scan_1 = "one"
scan_2 = "two"
x = 2
)foo");
TEST(LayerCommon, test_13)
{
     std::stringstream si_1(input_13_1);
     std::stringstream si_2(input_13_2);
     Layer l1(1);
     Layer l2(2);
     build(si_1, l1);
     build(si_2, l2);
     l1 << l2;
     std::stringstream so;
     str(l1, so);
     ASSERT_EQ(etalon_13, so.str());
     //std::cerr << so.str();
}

std::string input_13_4(R"foo(
[a.b."c.d.e.f"]
x = 1
)foo");
std::string etalon_13_4(R"foo(
[a]

[a.b]

[a.b."c.d.e.f"]
x = 1
)foo");
TEST(LayerCommon, test_13_2)
{
     std::stringstream si_1(input_13_4);
     Layer l1(1);
     build(si_1, l1);
     std::stringstream so;
     dump(l1, so);
     ASSERT_EQ(etalon_13_4, so.str());
     //std::cerr << so.str();
}

std::string input_14(R"foo(
[cli]
config = "filename"
)foo");
TEST(LayerCommon, test_14)
{
     std::stringstream si_1(input_14);
     Layer l1(1);
     build(si_1, l1);
     value_t v;
     get(l1, v, path_t{"cli", "config"});
     auto V = get<String>(v);
     ASSERT_TRUE(v);
     ASSERT_EQ(V.content, "filename");
}

std::string input_15(R"foo(
[cli]
config = "filename"
)foo");
TEST(LayerCommon, test_15)
{
     std::stringstream si_1(input_15);
     Layer l1(1);
     build(si_1, l1);
     value_t v;
     get(l1, v, path_t{"cli", "xconfig"});
     ASSERT_FALSE(v);
}

std::string input_16(R"foo(
config = "filename"
)foo");
TEST(LayerCommon, test_16)
{
     std::stringstream si_1(input_16);
     Layer l1(1);
     build(si_1, l1);
     value_t v;
     get(l1, v, path_t{"config"});
     auto V = get<String>(v);
     ASSERT_TRUE(v);
     ASSERT_EQ(V.content, "filename");
}

std::string input_17(R"foo(
config = "filename"
)foo");
TEST(LayerCommon, test_17)
{
     std::stringstream si_1(input_17);
     Layer l1(1);
     build(si_1, l1);
     value_t v;
     get(l1, v, path_t{"xconfig"});
     ASSERT_FALSE(v);
}

std::string input_18(R"foo(
[cli.a.b.c]
config = "filename"
)foo");
TEST(LayerCommon, test_18)
{
     std::stringstream si_1(input_18);
     Layer l1(1);
     build(si_1, l1);
     value_t v;
     get(l1, v, path_t{"cli", "a", "b", "c", "config"});
     auto V = get<String>(v);
     ASSERT_TRUE(v);
     ASSERT_EQ(V.content, "filename");
}

std::string input_19(R"foo(
[cli.a.b.c]
config = "filename"
)foo");
TEST(LayerCommon, test_19)
{
     std::stringstream si_1(input_19);
     Layer l1(1);
     build(si_1, l1);
     value_t v;
     get(l1, v, path_t{"cli", "a", "c", "config"});
     ASSERT_FALSE(v);
}

std::string input_20(R"foo(
[cli.a.b.c]
config = "filename"
)foo");
TEST(LayerCommon, test_20)
{
     std::stringstream si_1(input_20);
     Layer l1(1);
     build(si_1, l1);
     value_t v;
     get(l1, v, path_t{});
     ASSERT_FALSE(v);
}
