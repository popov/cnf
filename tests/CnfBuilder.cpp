#include <Builder.hpp>
#include <Stringer.hpp>
#include <gtest/gtest.h>

using namespace cnf;

TEST(Builder, test_01)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Finish>());
     dict_t dt;
     dt["a"] = get<value_t>(String("test"));

     Builder B(dt, evs);
     B.build();
     ASSERT_EQ(0, dt.size());
}

TEST(Builder, test_02)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Table>(event::keys_t{"one"}));
     evs.push_back(std::make_shared<event::Finish>());
     dict_t dt;
     dt["a"] = get<value_t>(String("test"));

     Builder B(dt, evs);
     B.build();
     ASSERT_EQ(1, dt.size());
}

TEST(Builder, test_03)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Table>(event::keys_t{"one"}));
     evs.push_back(std::make_shared<event::Table>(event::keys_t{
		    "a1", "b2", "c3",
			 }));
     evs.push_back(std::make_shared<event::Finish>());
     dict_t dt;
     dt["a"] = get<value_t>(String("test"));

     Builder B(dt, evs);
     B.build();
     ASSERT_EQ(2, dt.size());
}

TEST(Builder, test_04)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Table>(event::keys_t{"one"}));
     evs.push_back(std::make_shared<event::TablesArray>(event::keys_t{
		    "a1", "b2", "c3",
			 }));
     evs.push_back(std::make_shared<event::Finish>());
     dict_t dt;

     Builder B(dt, evs);
     B.build();
     ASSERT_EQ(2, dt.size());
}

std::string etalon_05(R"foo(key = 999

[[a1.b2.c3]]
)foo");
TEST(Builder, test_05)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Key>("key"));
     evs.push_back(std::make_shared<event::Integer>(999));
     evs.push_back(std::make_shared<event::Table>(event::keys_t{"one"}));
     evs.push_back(std::make_shared<event::TablesArray>(event::keys_t{
		    "a1", "b2", "c3",
			 }));
     evs.push_back(std::make_shared<event::Finish>());
     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(3, dt.size());
     ASSERT_EQ(etalon_05, ss.str());
}

std::string etalon_051(R"foo(key = 999

[a1]

[a1.b2]

[[a1.b2.c3]]

[one]
)foo");
TEST(Builder, test_051)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Key>("key"));
     evs.push_back(std::make_shared<event::Integer>(999));
     evs.push_back(std::make_shared<event::Table>(event::keys_t{"one"}));
     evs.push_back(std::make_shared<event::TablesArray>(event::keys_t{
		    "a1", "b2", "c3",
			 }));
     evs.push_back(std::make_shared<event::Finish>());
     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::DUMP);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(3, dt.size());
     ASSERT_EQ(etalon_051, ss.str());
}

std::string etalon_06(R"foo(
key = [
  1,
  2,
  3,
]
)foo");
TEST(Builder, test_06)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Key>("key"));
     evs.push_back(std::make_shared<event::StartArray>());
     evs.push_back(std::make_shared<event::Integer>(1));
     evs.push_back(std::make_shared<event::Integer>(2));
     evs.push_back(std::make_shared<event::Integer>(3));
     evs.push_back(std::make_shared<event::FinishArray>(3));
     evs.push_back(std::make_shared<event::Finish>());

     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(1, dt.size());
     ASSERT_EQ(etalon_06, ss.str());
}

std::string etalon_07(R"foo(
[one]

key = [
  1,
  2,
  3,
]
)foo");
TEST(Builder, test_07)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Table>(event::keys_t{"one"}));
     evs.push_back(std::make_shared<event::Key>("key"));
     evs.push_back(std::make_shared<event::StartArray>());
     evs.push_back(std::make_shared<event::Integer>(1));
     evs.push_back(std::make_shared<event::Integer>(2));
     evs.push_back(std::make_shared<event::Integer>(3));
     evs.push_back(std::make_shared<event::FinishArray>(3));
     evs.push_back(std::make_shared<event::Finish>());

     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(1, dt.size());
     ASSERT_EQ(etalon_07, ss.str());
}

std::string etalon_08(R"foo(
[one]

key = [ [
  1,
  2,
], [
  3,
  4,
  5,
], ]
)foo");
TEST(Builder, test_08)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Table>(event::keys_t{"one"}));
     evs.push_back(std::make_shared<event::Key>("key"));
     evs.push_back(std::make_shared<event::StartArray>());
     evs.push_back(std::make_shared<event::StartArray>());
     evs.push_back(std::make_shared<event::Integer>(1));
     evs.push_back(std::make_shared<event::Integer>(2));
     evs.push_back(std::make_shared<event::FinishArray>(2));
     evs.push_back(std::make_shared<event::StartArray>());
     evs.push_back(std::make_shared<event::Integer>(3));
     evs.push_back(std::make_shared<event::Integer>(4));
     evs.push_back(std::make_shared<event::Integer>(5));
     evs.push_back(std::make_shared<event::FinishArray>(3));
     evs.push_back(std::make_shared<event::FinishArray>(2));
     evs.push_back(std::make_shared<event::Finish>());

     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(1, dt.size());
     ASSERT_EQ(etalon_08, ss.str());
}

std::string etalon_09(R"foo(x = 99

[key0]
key1 = 1
key2 = 2
)foo");
TEST(Builder, test_09)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Key>("key0"));
     evs.push_back(std::make_shared<event::StartInline>());
     evs.push_back(std::make_shared<event::Key>("key1"));
     evs.push_back(std::make_shared<event::Integer>(1));
     evs.push_back(std::make_shared<event::Key>("key2"));
     evs.push_back(std::make_shared<event::Integer>(2));
     evs.push_back(std::make_shared<event::FinishInline>(2));
     evs.push_back(std::make_shared<event::Key>("x"));
     evs.push_back(std::make_shared<event::Integer>(99));
     evs.push_back(std::make_shared<event::Finish>());

     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(2, dt.size());
     ASSERT_EQ(etalon_09, ss.str());
}

std::string etalon_10(R"foo(
[one]
x = 99

[one.key0]
key1 = 1
key2 = 2
)foo");
TEST(Builder, test_10)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Table>(event::keys_t{"one"}));
     evs.push_back(std::make_shared<event::Key>("key0"));
     evs.push_back(std::make_shared<event::StartInline>());
     evs.push_back(std::make_shared<event::Key>("key1"));
     evs.push_back(std::make_shared<event::Integer>(1));
     evs.push_back(std::make_shared<event::Key>("key2"));
     evs.push_back(std::make_shared<event::Integer>(2));
     evs.push_back(std::make_shared<event::FinishInline>(2));
     evs.push_back(std::make_shared<event::Key>("x"));
     evs.push_back(std::make_shared<event::Integer>(99));
     evs.push_back(std::make_shared<event::Finish>());

     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(1, dt.size());
     ASSERT_EQ(etalon_10, ss.str());
}

std::string etalon_11(R"foo(
[one]
x = 99

[one.key0]
key1 = 1

key2 = [
  3,
  4,
  5,
]
)foo");
TEST(Builder, test_11)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::Table>(event::keys_t{"one"}));
     evs.push_back(std::make_shared<event::Key>("key0"));
     evs.push_back(std::make_shared<event::StartInline>());
     evs.push_back(std::make_shared<event::Key>("key1"));
     evs.push_back(std::make_shared<event::Integer>(1));
     evs.push_back(std::make_shared<event::Key>("key2"));
     evs.push_back(std::make_shared<event::StartArray>());
     evs.push_back(std::make_shared<event::Integer>(3));
     evs.push_back(std::make_shared<event::Integer>(4));
     evs.push_back(std::make_shared<event::Integer>(5));
     evs.push_back(std::make_shared<event::FinishArray>(3));
     evs.push_back(std::make_shared<event::FinishInline>(2));
     evs.push_back(std::make_shared<event::Key>("x"));
     evs.push_back(std::make_shared<event::Integer>(99));
     evs.push_back(std::make_shared<event::Finish>());

     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(1, dt.size());
     ASSERT_EQ(etalon_11, ss.str());
}

std::string etalon_12(R"foo(
[[fruit]]
name = "apple"

[[fruit.variety]]
name = "red delicious"

[[fruit.variety]]
name = "granny smith"

[fruit.physical]
color = "red"
shape = "round"

[[fruit]]
name = "bananna"

[[fruit.variety]]
name = "plantain"
)foo");
TEST(Builder, test_12)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::TablesArray>
		   (event::keys_t{"fruit"}));
     
          evs.push_back(std::make_shared<event::Key>("name"));
     evs.push_back(std::make_shared<event::String>("apple"));
     
     evs.push_back(std::make_shared<event::Table>
		   (event::keys_t{"fruit", "physical"}));

     evs.push_back(std::make_shared<event::Key>("color"));
     evs.push_back(std::make_shared<event::String>("red"));
     evs.push_back(std::make_shared<event::Key>("shape"));
     evs.push_back(std::make_shared<event::String>("round"));

     
     evs.push_back(std::make_shared<event::TablesArray>
		   (event::keys_t{"fruit", "variety"}));

     evs.push_back(std::make_shared<event::Key>("name"));
     evs.push_back(std::make_shared<event::String>("red delicious"));
     
     evs.push_back(std::make_shared<event::TablesArray>
		   (event::keys_t{"fruit", "variety"}));
     
     evs.push_back(std::make_shared<event::Key>("name"));
     evs.push_back(std::make_shared<event::String>("granny smith"));

     evs.push_back(std::make_shared<event::TablesArray>
		   (event::keys_t{"fruit"}));
     evs.push_back(std::make_shared<event::Key>("name"));
     evs.push_back(std::make_shared<event::String>("bananna"));
     evs.push_back(std::make_shared<event::TablesArray>
		   (event::keys_t{"fruit", "variety"}));
     evs.push_back(std::make_shared<event::Key>("name"));
     evs.push_back(std::make_shared<event::String>("plantain"));
     
     evs.push_back(std::make_shared<event::Finish>());

     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(1, dt.size());
     ASSERT_EQ(etalon_12, ss.str());
}

std::string etalon_13(R"foo(
[[fruit]]
name = "bananna"

[[fruit.variety]]
name = "red delicious"

[[fruit]]
name = "bananna"

[[fruit.variety]]
name = "plantain"
)foo");
TEST(Builder, test_13)
{
     event::events_t evs;
     evs.push_back(std::make_shared<event::Start>());
     evs.push_back(std::make_shared<event::TablesArray>
     (event::keys_t{"fruit"}));

     //evs.push_back(std::make_shared<event::TablesArray>
     //(event::keys_t{"fruit"}));
     //evs.push_back(std::make_shared<event::TablesArray>
     //	   (event::keys_t{"fruit"}));

     evs.push_back(std::make_shared<event::Key>("name"));
     evs.push_back(std::make_shared<event::String>("bananna"));
     
     evs.push_back(std::make_shared<event::TablesArray>
		   (event::keys_t{"fruit", "variety"}));
          evs.push_back(std::make_shared<event::Key>("name"));
     evs.push_back(std::make_shared<event::String>("red delicious"));

     evs.push_back(std::make_shared<event::TablesArray>
		   (event::keys_t{"fruit"}));
     evs.push_back(std::make_shared<event::Key>("name"));
     evs.push_back(std::make_shared<event::String>("bananna"));



     evs.push_back(std::make_shared<event::TablesArray>
		   (event::keys_t{"fruit", "variety"}));
     evs.push_back(std::make_shared<event::Key>("name"));
     evs.push_back(std::make_shared<event::String>("plantain"));

     //evs.push_back(std::make_shared<event::TablesArray>
     //(event::keys_t{"fruit"}));
     evs.push_back(std::make_shared<event::Finish>());

     dict_t dt;
     Builder B(dt, evs);
     B.build();

     //TODO
     std::stringstream ss;
     DictStringer DS(Stringer::UNIX, 2, Stringer::REGULAR);
     DS.fill(dt, ss);
     //std::cerr << ss.str();
     //

     ASSERT_EQ(1, dt.size());
     auto R = dt.at("fruit");
     Array* AR = (Array*)(R.get());
     ASSERT_EQ(2, AR->content.size());
     ASSERT_EQ(etalon_13, ss.str());
}
